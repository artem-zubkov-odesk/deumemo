0) generate xsd then classes using xjc

XML Processing: From xsd to java:
mkdir generated
xjc -d generated dictionary.xsd
xjc -d generated -p com.martiazoo.tort.deumemo.logic.xml.generated dictionary.xsd
rm generated/com/martiazoo/tort/deumemo/logic/xml/generated/ObjectFactory.java

1) create xml w/o attributes
2) deletee objectfactory class as unused
3) find & replace commnets:
/\*\*(?s:(?!\*/).)*\*/
4) find & replace annotations:
@.+
5) get rid of wrapper classes, put their inner List<ABC> abcs; list directly into container class
6) annotate root with @Default
7) delete wrapper classes
8) done
