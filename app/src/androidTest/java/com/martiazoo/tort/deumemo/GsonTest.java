package com.martiazoo.tort.deumemo;


import android.test.InstrumentationTestCase;

import com.martiazoo.tort.deumemo.logic.xml.generated.DictionaryType;
import com.stanfy.gsonxml.GsonXml;
import com.stanfy.gsonxml.GsonXmlBuilder;
import com.stanfy.gsonxml.XmlParserCreator;

import org.simpleframework.xml.core.Persister;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class GsonTest extends InstrumentationTestCase {
    public GsonTest() {
        super();
    }

    public static class SimpleModel {
        private String name;
        private String description;

        public String getName() {
            return name;
        }

        public String getDescription() {
            return description;
        }
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public void testSimpleTestFromString() {
        XmlParserCreator parserCreator = new XmlParserCreator() {
            @Override
            public XmlPullParser createParser() {
                try {
                    return XmlPullParserFactory.newInstance().newPullParser();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };

        GsonXml gsonXml = new GsonXmlBuilder()
                .setXmlParserCreator(parserCreator)
                .create();

        String xml = "<model><name>my name</name><description>my description</description></model>";
        SimpleModel model = gsonXml.fromXml(xml, SimpleModel.class);
        System.err.println("model: " + model);
        System.err.println("model: " + model);
        assertEquals("my name", model.getName());
        assertEquals("my description", model.getDescription());
    }

    public void testSimpleTestFromFile() throws IOException {
        XmlParserCreator parserCreator = new XmlParserCreator() {
            @Override
            public XmlPullParser createParser() {
                try {
                    return XmlPullParserFactory.newInstance().newPullParser();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };

        GsonXml gsonXml = new GsonXmlBuilder()
                .setXmlParserCreator(parserCreator)
                .create();
//getInstrumentation().getTargetContext().getResources().getAssets().open(testFile);
        InputStream is = getInstrumentation().getContext().getAssets().open("test.xml");
        Reader r = new InputStreamReader(is);
        SimpleModel model = gsonXml.fromXml(r, SimpleModel.class);
        System.err.println("model: " + model);
        assertEquals("my name", model.getName());
        assertEquals("my description", model.getDescription());
    }

    public void testXmlStructuredddddd() throws IOException {
        XmlParserCreator parserCreator = new XmlParserCreator() {
            @Override
            public XmlPullParser createParser() {
                try {
                    return XmlPullParserFactory.newInstance().newPullParser();
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };

        GsonXml gsonXml = new GsonXmlBuilder()
                .setXmlParserCreator(parserCreator)
                .create();

        InputStream is = getInstrumentation().getContext().getAssets().open("dictionary.xml");
//        Reader r = new InputStreamReader(is);
//        String s = r.toString();
        String s = convertStreamToString(is);
        DictionaryType object = gsonXml.fromXml(s, DictionaryType.class);
        System.err.println("model: " + object);
        System.err.println("model: " + object);
//        assertEquals("my name", model.getName());
//        assertEquals("my description", model.getDescription());
    }

    public void testXmlFromSimpleframework() throws Exception {
        InputStream is = getInstrumentation().getContext().getAssets().open("dictionary.xml");
        Persister persister = new Persister();
        DictionaryType object = persister.read(DictionaryType.class, is);
        System.err.println("model: " + object);
    }
}
