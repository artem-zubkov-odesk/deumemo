package com.martiazoo.tort.deumemo;

import android.graphics.Point;

public class AspectRatioCalculator {

    /**
     * Calculate size of matrix, which requires to keep <b>n</b> elements.
     * The factor is 1.5.
     * Width is always longer or equal to height.
     * This is because landscape orientation is expected.
     * <p/>
     * <i>Example</i>
     * if N=24 resulted point.x=6; point.y=4
     */
    public static Point calculateMatrixDimensions(int n) {
        double w = Math.sqrt(3 * n / 2);
        double h = w * 2 / 3;

        w = Math.floor(w);
        h = Math.floor(h);

        int interrupt = 0;
        while (w * h < n) {
            interrupt++;
            if (interrupt > 10) {
                break;
            }
            if (w % 2 != 0) {
                w++;
                continue;
            }
            if (w == h) {
                w++;
                continue;
            }
            h++;
        }
        if (w < h) {
            throw new RuntimeException("w < h");
        }
//        if (w * h > n) {
//            throw new RuntimeException("w * h > n");
//        }
        return new Point((int) w, (int) h);
    }

}
