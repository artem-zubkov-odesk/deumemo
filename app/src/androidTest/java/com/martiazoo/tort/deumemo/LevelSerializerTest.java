package com.martiazoo.tort.deumemo;

import android.graphics.Point;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.Suppress;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.logic.level.LevelSerializer;
import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;
import junit.framework.Assert;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.util.ArrayList;
import java.util.List;

public class LevelSerializerTest extends InstrumentationTestCase {
    public LevelSerializerTest() {
        super();
    }

    public void testNotNull() {
        Level l = new Level(getInstrumentation().getContext());
        String s = LevelSerializer.instance.toString(l);
        Assert.assertNotNull(s);
    }

    public void testVersion() {
        Level l = new Level(getInstrumentation().getContext());
        String s = LevelSerializer.instance.toString(l);
        Assert.assertTrue(s.contains("version1"));
    }

    public void testExcludes() {
        Level l = new Level(getInstrumentation().getContext());
        String s = LevelSerializer.instance.toString(l);

        Assert.assertFalse(s.contains("levelController"));
        Assert.assertFalse(s.contains("ctx"));
//        Assert.assertFalse(s.contains("elements"));
        Assert.assertFalse(s.contains("cardTextRenderer"));
        Assert.assertFalse(s.contains("cardBitmapRenderer"));
        Assert.assertFalse(s.contains("skipViewClicks"));
        Assert.assertFalse(s.contains("elementClickListener"));
        Assert.assertFalse(s.contains("cardTableClickListener"));
    }

    public void testIncludes() throws IllegalAccessException {
        Level l = new Level(getInstrumentation().getContext());

        Point dimension = new Point(1, 2);
        FieldUtils.getField(l.getClass(), "dimension", true).set(l, dimension);

        List<PairType> pairs = new ArrayList<>();
        PairType pairType = new PairType();
        pairType.setA("A");
        pairType.setB("B");
        pairs.add(pairType);
        FieldUtils.getField(l.getClass(), "pairs", true).set(l, pairs);

        String s = LevelSerializer.instance.toString(l);

        Assert.assertTrue(s.contains("pairs"));
        Assert.assertTrue(s.contains("dimension"));
        Assert.assertTrue(s.contains("levelCompleteInfo"));
    }

    @Suppress
    public void testFromString() throws IllegalAccessException {
        LevelSerializer.HolderVersion1 holder = LevelSerializer.instance.fromString(dummyLevel());
        Level l = new LevelSerializer.StateLevelBuilder(getInstrumentation().getContext(), holder).nextLevel();
        Assert.assertNotNull(l);
        Assert.assertNotNull(l.getPairs());
        Assert.assertFalse(l.getPairs().isEmpty());
        Assert.assertEquals(l.getPairs().get(0).getA(), "A");
        Assert.assertEquals(l.getPairs().get(0).getB(), "B");
    }

    String dummyLevel() {
        return "<version1>\n" +
                "  <dimension>\n" +
                "    <x>1</x>\n" +
                "    <y>2</y>\n" +
                "  </dimension>\n" +
                "  <levelCompleteInfo>\n" +
                "    <durationMs>0</durationMs>\n" +
                "    <startedMs>1465052600730</startedMs>\n" +
                "    <correctAttempts>0</correctAttempts>\n" +
                "    <incorrectAttempts>0</incorrectAttempts>\n" +
                "  </levelCompleteInfo>\n" +
                "  <pairs>\n" +
                "    <pair>\n" +
                "      <a>A</a>\n" +
                "      <b>B</b>\n" +
                "    </pair>\n" +
                "  </pairs>\n" +
                "\n</version1>";
    }

//    <version1>
//    <dimension>
//    <x>2</x>
//    <y>4</y>
//    </dimension>
//    <elements>
//    <GFElement>
//    <cardState>HIDDEN</cardState>
//    <pair>
//    <a>verdattert</a>
//    <b>обалделый</b>
//    </pair>
//    <isA>true</isA>
//    <position>0</position>
//    </GFElement>
//    <GFElement>
//    <cardState>BACK</cardState>
//    <pair>
//    <a>назойливый</a>
//    <b>zudringlich</b>
//    </pair>
//    <isA>false</isA>
//    <position>1</position>
//    </GFElement>
//    <GFElement>
//    <cardState>BACK</cardState>
//    <pair>
//    <a>bestürzt</a>
//    <b>в замешательстве</b>
//    </pair>
//    <isA>false</isA>
//    <position>2</position>
//    </GFElement>
//    <GFElement>
//    <cardState>BACK</cardState>
//    <pair reference="../../GFElement[3]/pair"/>
//    <isA>true</isA>
//    <position>3</position>
//    </GFElement>
//    <GFElement>
//    <cardState>BACK</cardState>
//    <pair>
//    <a>дурной, злой</a>
//    <b>arg</b>
//    </pair>
//    <isA>false</isA>
//    <position>4</position>
//    </GFElement>
//    <GFElement>
//    <cardState>HIDDEN</cardState>
//    <pair reference="../../GFElement/pair"/>
//    <isA>false</isA>
//    <position>5</position>
//    </GFElement>
//    <GFElement>
//    <cardState>BACK</cardState>
//    <pair reference="../../GFElement[2]/pair"/>
//    <isA>true</isA>
//    <position>6</position>
//    </GFElement>
//    <GFElement>
//    <cardState>BACK</cardState>
//    <pair reference="../../GFElement[5]/pair"/>
//    <isA>true</isA>
//    <position>7</position>
//    </GFElement>
//    </elements>
//    <levelCompleteInfo>
//    <startedMs>1466334802282</startedMs>
//    <durationMs>0</durationMs>
//    <incorrectAttempts>3</incorrectAttempts>
//    <correctAttempts>1</correctAttempts>
//    </levelCompleteInfo>
//    </version1>
}
