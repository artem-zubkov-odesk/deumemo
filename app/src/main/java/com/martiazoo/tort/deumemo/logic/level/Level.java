package com.martiazoo.tort.deumemo.logic.level;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.controller.LevelController;
import com.martiazoo.tort.deumemo.logic.level.done.LevelCompleteInfo;
import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;
import com.martiazoo.tort.deumemo.ui.card.CardBitmapRenderer;
import com.martiazoo.tort.deumemo.ui.card.CardTextRenderer;
import com.martiazoo.tort.deumemo.ui.card.CardView;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Level {
    private static final String TAG = "Level";
    public final LevelController levelController = LevelController.Instance.get();
    private Context ctx;
    private final List<GFElement> elements = new ArrayList<>();
    private CardTextRenderer cardTextRenderer;
    private CardBitmapRenderer cardBitmapRenderer;
    public boolean skipViewClicks = true;
    private final View.OnClickListener elementClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (skipViewClicks) {
                return;
            }
            if (!(v instanceof CardView)) {
                return;
            }
            CardView view = (CardView) v;
            levelController.elementClicked(view.getElement());
        }
    };
    public final View.OnClickListener cardTableClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            levelController.tableClicked();
        }
    };

    private LevelCompleteInfo levelCompleteInfo = new LevelCompleteInfo();
    private Point dimension;
    private List<PairType> pairs;

    public LevelCompleteInfo getLevelCompleteInfo() {
        return levelCompleteInfo;
    }

    public void setLevelCompleteInfo(LevelCompleteInfo levelCompleteInfo) {
        this.levelCompleteInfo = levelCompleteInfo;
    }

    public Level(Context ctx) {
        this.ctx = ctx;
    }

    public void init(Point dimension, List<PairType> pairs, @Nullable List<GFElement> list) {
        Log.d(TAG, "init called: ");
        this.dimension = dimension;
        this.pairs = pairs;
        if (list != null) {
            populateFromList(list);
        } else {
            populate(pairs);
        }
        cardTextRenderer = CardTextRenderer.getInstance(ctx);
        cardBitmapRenderer = CardBitmapRenderer.getInstance((Activity) ctx);
    }

    public void populateFromList(List<GFElement> list) {
        elements.addAll(Collections.<GFElement>nCopies(list.size(), null));
        for (GFElement e : list) {
            GFElement copy = e.copy();
            elements.set(copy.getPosition(), copy);
        }
    }

    public void populate(List<PairType> pairs) {
        for (PairType pairType : pairs) {
            elements.add(new GFElement(pairType, true));
            elements.add(new GFElement(pairType, false));
        }
        Collections.shuffle(elements);
        int i = 0;
        for (GFElement gfElement : elements) {
            gfElement.setPosition(i);
            i++;
        }
    }

    public void setViewResolver(ViewResolver viewResolver) {
        levelController.initialize(ctx, this, elements, viewResolver);
    }

    public int getSize() {
        return dimension.x * dimension.y;
    }

    public Point getPuzzleDimension() {
        return dimension;
    }

    public GFElement getItem(int position) {
        return elements.get(position % elements.size());
    }

    public List<PairType> getPairs() {
        return pairs;
    }

    public List<GFElement> getElements() {
        return Collections.unmodifiableList(elements);
    }

    public View constructView(int position) {
        GFElement p = getItem(position);
        Log.d(TAG, "getView called for position: " + position);
        View v;
        {
            CardView view = new CardView(ctx, cardTextRenderer, cardBitmapRenderer);
//            GFTextView view = new DebugNumberedTextView((Context) this.getLevelScope().getValues().get("context"));
            view.setElement(p);
            view.setPosition(p.getPosition());
            view.init();
            v = view;
        }
        v.setOnClickListener(this.elementClickListener);
        return v;
    }
}
