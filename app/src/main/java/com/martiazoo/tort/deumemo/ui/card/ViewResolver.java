package com.martiazoo.tort.deumemo.ui.card;

import android.view.ViewGroup;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;

import java.util.LinkedHashMap;
import java.util.Map;

public class ViewResolver {
    private ViewGroup tableLayout;
    private Map<GFElement, CardView> cached = new LinkedHashMap<>();

    public ViewResolver(Level level, ViewGroup gridview) {
//        this.level = level;
        this.tableLayout = gridview;
    }

    private CardView getViewByPosition(int position) {
        if (position == -1 || tableLayout.getChildCount() <= position) {
            throw new IllegalArgumentException();
        }
        return (CardView) tableLayout.getChildAt(position);
    }

    public CardView toView(GFElement e) {
        CardView v = cached.get(e);
        if (v == null) {
            v = getViewByPosition(e.getPosition());
            cached.put(e, v);
        }
        return v;
    }
}
