package com.martiazoo.tort.deumemo.logic.dictionary;

import android.content.Context;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class Dictionaries {
    private Context ctx;
    private DictionaryFactory df;
    private Dictionary d;

    public Dictionaries(Context ctx) {
        this.ctx = ctx;
    }

    public void init() {
        if (df != null) {
            return;
        }
        df = new DictionaryFactory(ctx, "ru_de");
    }

    public Map<String, String> getFilenameDisplayName() {
        return df.getFilenameDisplayName();
    }

    public String getDefaultFilename() {
        return df.getDefaultFilename();
    }

    public void updateSelectedDictionary(List<String> filenames) {
        d = df.createDictionary(filenames);
    }

    public Dictionary getCurrentDictionary() {
        return d;
    }
    public String getRootName() {
        return df.subDirectory;
    }
}
