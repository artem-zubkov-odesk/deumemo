package com.martiazoo.tort.deumemo.logic.level.done;

import com.martiazoo.tort.deumemo.R;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class LevelCompleteInfo {
    public long startedMs = System.currentTimeMillis();
    public int incorrectAttempts;
    public int correctAttempts;

    public static LevelCompleteInfo nextDummy() {
        LevelCompleteInfo levelCompleteInfo = new LevelCompleteInfo();
        levelCompleteInfo.correctAttempts = 123;
        levelCompleteInfo.incorrectAttempts = 407;
        return levelCompleteInfo;
    }

    public List<Element> toElements() {
        long durationMs = System.currentTimeMillis() - startedMs;
        return Arrays.asList(
                new Element(String.valueOf(millisToHHMMSS(durationMs)), R.drawable.win_icon_clock),
                new Element(String.valueOf(incorrectAttempts), R.drawable.win_icon_bad),
                new Element(String.valueOf(correctAttempts), R.drawable.win_icon_good)
        );
    }

    static String millisToHHMMSS(long millis) {
        String s = String.format("%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                TimeUnit.MILLISECONDS.toSeconds(millis) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        return s;
    }

    public static class Element {
        public final String text;
        public final int drawableResourceId;

        public Element(String text, int drawableResourceId) {
            this.text = text;
            this.drawableResourceId = drawableResourceId;
        }
    }
}
