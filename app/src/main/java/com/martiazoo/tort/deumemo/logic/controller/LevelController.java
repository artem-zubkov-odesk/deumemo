package com.martiazoo.tort.deumemo.logic.controller;

import android.content.Context;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.ui.card.CardView;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;

import java.util.List;

/**
 * one controller for whole game lifecycle
 */
public interface LevelController {
    void initialize(Context ctx, Level l, List<GFElement> all, ViewResolver vr);

    void animatePopulation(List<CardView> all, final Runnable callback);

    void heartbeat();

    void elementClicked(GFElement e);

    void tableClicked();

    void destroy(final Runnable... callback);

    /**
     * can be hanges only by compilation
     */
    class Instance {
        public static LevelController get() {
            return new TwoPassFlow();
//            return new TwoPassAutoplayRobot();
//            return new DebugAllOpened();
//            return new DebugFlipCard();
        }
    }
}
