package com.martiazoo.tort.deumemo.ui.actionbar;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.Utils;
import com.martiazoo.tort.deumemo.logic.dictionary.Dictionaries;
import com.martiazoo.tort.deumemo.logic.scope.ConfigProvider;
import com.martiazoo.tort.deumemo.logic.scope.LevelSize;
import com.martiazoo.tort.deumemo.ui.cardtable.ActivitySwipeDetector;
import com.martiazoo.tort.deumemo.ui.wordlist.VisibilityListener;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;

public class MenuHelper {
    private static final String TAG = "MenuHelper";
    private VisibilityListener visibilityListener = VisibilityListener.DUMMY;
    private final ActionBarActivity activity;
    private final Runnable shuffleLevelCallable;
    private final Dictionaries dictionaries;
    private final ConfigProvider configProvider;

    private View actionBarUnderlyingBackground;
    private boolean isUnderlyingBgShown = true;
    private Animation topToBottomAnimation;
    private Animation bottomToTopAnimation;
    private Point displaySize = new Point(100, 100);

    public MenuHelper(ActionBarActivity activity, Dictionaries dictionaries, ConfigProvider configProvider, Runnable shuffleLevelCallable) {
        this.activity = activity;
        this.dictionaries = dictionaries;
        this.configProvider = configProvider;
        this.shuffleLevelCallable = shuffleLevelCallable;
    }

    public void setVisibilityListener(VisibilityListener visibilityListener) {
        this.visibilityListener = visibilityListener;
    }

    public int getActionBarFullHeight() {
        return Utils.getActionBarHeight(activity) + displaySize.y / 20;
    }

    public void onCreate(Bundle savedInstanceState) {
        {
            Display display = activity.getWindowManager().getDefaultDisplay();
            display.getSize(displaySize);
        }
        actionBarUnderlyingBackground = (View) activity.findViewById(R.id.actionBarUnderlyingView);
        topToBottomAnimation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.move_from_top_to_bottom);
        bottomToTopAnimation = AnimationUtils.loadAnimation(activity.getApplicationContext(), R.anim.move_from_bottom_to_top);
        ViewGroup.LayoutParams params = actionBarUnderlyingBackground.getLayoutParams();
        params.height = getActionBarFullHeight();
        actionBarUnderlyingBackground.setLayoutParams(params);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
//        Log.d(TAG, "onCreateOptionsMenu: called : " + menu);
        activity.getMenuInflater().inflate(R.menu.menu_main, menu);
        updateLevelSizeMenuList(menu);
        updateMenuHintImageSize();
        return false;
    }

    public void optionDictionaryClicked(android.view.MenuItem mi) {
        updateDictionaryTypeMenuList(mi.getSubMenu());
    }

    public void menuSwipeHintTopClicked(View v) {
        if (isShowing()) {
            hideActionBar();
        } else {
            showActionBar();
        }
    }

    public void updateMenuHintImageSize() {
        final ImageView iv = (ImageView) activity.findViewById(R.id.menuSwipeHintTop);
        updateMenuHintImageSize(iv);
    }

    public void updateMenuHintImageSize(final ImageView iv) {
        Point size = topMenuHintButtonSize();
        ViewGroup.LayoutParams params = iv.getLayoutParams();
        params.width = size.x;
        params.height = size.y;
        iv.setLayoutParams(params);
    }

    private Point topMenuHintButtonSize() {
        Point displaySize = new Point();
        Display display = activity.getWindowManager().getDefaultDisplay();
        display.getSize(displaySize);
        Point size = new Point();
        size.y = (int) (Utils.getActionBarHeight(activity) * 0.7d);

        Drawable drawable = activity.getResources().getDrawable(R.drawable.menu_swipe_hint_top_img_v2);
        double factor = ((double) drawable.getMinimumWidth()) / drawable.getMinimumHeight();
        size.x = (int) (factor * size.y);
        return size;
    }

    public void hideActionBar() {
        try {
            if (isUnderlyingBgShown) {
                isUnderlyingBgShown = false;
                actionBarUnderlyingBackground.startAnimation(bottomToTopAnimation);
                actionBarUnderlyingBackground.setVisibility(View.INVISIBLE);
            }

            android.support.v7.app.ActionBar supportActionBar = activity.getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.hide();
                return;
            }
        } finally {
            visibilityListener.notifyVisibilityChanged(false);
        }
    }

    private void showActionBar() {
        try {
            if (!isUnderlyingBgShown) {
                isUnderlyingBgShown = true;
                actionBarUnderlyingBackground.setVisibility(View.VISIBLE);
                actionBarUnderlyingBackground.startAnimation(topToBottomAnimation);
            }

            android.support.v7.app.ActionBar supportActionBar = activity.getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.show();
                return;
            }
        } finally {
            visibilityListener.notifyVisibilityChanged(true);
        }
    }

    private void updateLevelSizeMenuList(Menu menu) {
        LevelSize selected = configProvider.getSelectedLevelSize();
        SubMenu submenu = menu.findItem(R.id.action_level_size).getSubMenu();
        for (LevelSize ls : LevelSize.values()) {
            MenuItem mi = submenu.add(R.id.menu_level_size_group, Menu.NONE, Menu.NONE, ls.name());
            if (ls == selected) {
                mi.setChecked(true);
            }
            mi.setOnMenuItemClickListener(levelSizeMenuListener);
        }
        submenu.setGroupCheckable(R.id.menu_level_size_group, true, true);
    }

    public boolean isShowing() {
        boolean isShowing = false;
        android.support.v7.app.ActionBar supportActionBar = activity.getSupportActionBar();
        if (supportActionBar != null) {
            if (supportActionBar.isShowing()) {
                isShowing = true;
            }
        }
        return isShowing;
    }

    private void updateDictionaryTypeMenuList(SubMenu subMenu) {
        subMenu.clear();
        List<String> chosen = configProvider.getChoosenDictionaries(
                dictionaries.getRootName(),
                dictionaries.getDefaultFilename());

        for (Map.Entry<String, String> entry : dictionaries.getFilenameDisplayName().entrySet()) {
            String filename = entry.getKey();
            String uiText = entry.getValue();
            MenuItem mi = subMenu.add(R.id.menu_dictionary_type_group, Menu.NONE, Menu.NONE, uiText);
            mi.setCheckable(true);
            mi.setOnMenuItemClickListener(new DictionaryTypeMenuListener(filename, subMenu));
            if (chosen.contains(filename)) {
                mi.setChecked(true);
            }
        }
        subMenu.setGroupCheckable(R.id.menu_dictionary_type_group, true, false);
    }

    private class DictionaryTypeMenuListener implements MenuItem.OnMenuItemClickListener {
        final String file;
        final SubMenu parent;

        public DictionaryTypeMenuListener(String file, SubMenu parent) {
            this.file = file;
            this.parent = parent;
        }

        @Override
        public boolean onMenuItemClick(MenuItem mi) {
            List<String> checked = calculateCheckedFilenames(mi);
            dictionaries.updateSelectedDictionary(checked);
            configProvider.setChoosenDictionaries(dictionaries.getRootName(), checked);
            shuffleLevelCallable.run();
            activity.invalidateOptionsMenu();
            activity.supportInvalidateOptionsMenu();
            return true;
        }

        List<String> calculateCheckedFilenames(MenuItem mi) {
            String current = mi.getTitle().toString();
            {
                List<String> tmp = titlesToFilenames(Arrays.asList(current));
                if (CollectionUtils.isEmpty(tmp)) {
                    current = null;
                } else {
                    current = tmp.get(0);
                }
            }
            List<String> checked = new ArrayList<>(configProvider.getChoosenDictionaries(
                    dictionaries.getRootName(), dictionaries.getDefaultFilename()));
            if (current == null) {
                return checked;
            }
            if (checked.contains(current)) {
                checked.remove(current);
            } else {
                checked.add(current);
            }
            if (checked.isEmpty()) {
                checked.add(current);
            }
            return checked;
        }

        List<String> titlesToFilenames(List<String> titles) {
            Map<String, String> reversed = new LinkedHashMap<>();
            for (Map.Entry<String, String> entry : dictionaries.getFilenameDisplayName().entrySet()) {
                reversed.put(entry.getValue(), entry.getKey());
            }
            List<String> filenames = new ArrayList<>();
            for (String title : titles) {
                filenames.add(reversed.get(title));
            }
            return filenames;
        }
    }

    private final MenuItem.OnMenuItemClickListener levelSizeMenuListener = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem mi) {
            LevelSize chosen = LevelSize.valueOf(mi.getTitle().toString());
            if (chosen == configProvider.getSelectedLevelSize()) {
                return true;
            }
            configProvider.setSelectedLevelSize(chosen);
            shuffleLevelCallable.run();
            activity.invalidateOptionsMenu();
            activity.supportInvalidateOptionsMenu();
            return true;
        }
    };

    public class ShowActionBarListener extends ActivitySwipeDetector {
        @Override
        public boolean onBottomToTopSwipe() {
            super.onBottomToTopSwipe();
            if (isShowing()) {
                hideActionBar();
            } else {
                showActionBar();
            }
            return true;
        }

        @Override
        public boolean onTopToBottomSwipe() {
            super.onTopToBottomSwipe();
            if (isShowing()) {
                hideActionBar();
            } else {
                showActionBar();
            }
            return true;
        }

        @Override
        public boolean onSimpleTap() {
            if (isShowing()) {
                super.onSimpleTap();
                hideActionBar();
                return true;
            }
            return false;
        }
    }
}