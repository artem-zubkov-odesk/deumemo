package com.martiazoo.tort.deumemo.logic.level;

import android.content.Context;

import com.martiazoo.tort.deumemo.logic.dictionary.Dictionary;
import com.martiazoo.tort.deumemo.logic.scope.LevelSize;

import java.util.ArrayList;
import java.util.List;

/*todo debug purposes*/
public class DebugReverseDictionaryLevelBuilder extends DictionaryLevelBuilder {
    private Level latest;
    private List<Level> levels = new ArrayList<>();

    public DebugReverseDictionaryLevelBuilder(Dictionary d, LevelSize ls, Context context) {
        super(d, ls, context);
    }

    public Level nextLevel() {
        if (latest != null && levels.size() > 0 && levels.get(levels.size() - 1) != latest) {
            int idx = levels.indexOf(latest);
            idx++;
            Level l = levels.get(idx);
            latest = l;
            return l;
        }
        Level l = super.nextLevel();
        latest = l;
        levels.add(l);
        return l;
    }

    public Level previousLevel() {
        if (latest == null) {
            return null;
        }
        if (levels.size() == 0) {
            return null;
        }
        if (levels.get(0) == latest) {
            return null;
        }
        int idx = levels.indexOf(latest);
        idx--;
        Level l = levels.get(idx);
        latest = l;
        return l;
    }
}
