package com.martiazoo.tort.deumemo.util.threading.forceable;

import android.os.Handler;
import com.martiazoo.tort.deumemo.util.threading.NullableRunnable;
import com.martiazoo.tort.deumemo.util.threading.StepLoadTracker;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class ForceablePool {
    private final ConcurrentHashMap<Object, ForceableRunnableSequence> map = new ConcurrentHashMap<>();
    private Handler handler;

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public void forceForeverAll(Runnable... done) {
        throwInitializetion();
        Collection<ForceableRunnableSequence> sequences = map.values();
        StepLoadTracker slt = new StepLoadTracker(sequences.size(), NullableRunnable.getFirst(done), 20000, handler);
        slt.waitLoading();
        for (ForceableRunnableSequence s : sequences) {
            s.forceForever(slt);
        }
    }

    public boolean forceAll(Runnable... done) {
        throwInitializetion();
        Collection<ForceableRunnableSequence> sequences = map.values();
        StepLoadTracker slt = new StepLoadTracker(sequences.size(), NullableRunnable.getFirst(done), 20000, handler);
        slt.waitLoading();
        boolean forced = false;
        for (ForceableRunnableSequence s : sequences) {
            boolean result = s.force(slt);
            forced = forced || result;
        }
        return forced;
    }

    //    @NotNull
    public ForceableRunnableSequence getSequence(Object key) {
        throwInitializetion();
        ForceableRunnableSequence s = map.get(key);
        if (s == null) {
            map.putIfAbsent(key, new ForceableRunnableSequence(handler));
            return map.get(key);
        } else {
            return s;
        }
    }

    private void throwInitializetion() {
        if (handler == null) {
            throw new RuntimeException("handler == null");
        }
    }
}
