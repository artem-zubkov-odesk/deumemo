package com.martiazoo.tort.deumemo.logic.level;

import android.content.Context;
import android.os.Bundle;
import com.martiazoo.tort.deumemo.logic.dictionary.Dictionary;
import com.martiazoo.tort.deumemo.logic.scope.LevelSize;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public abstract class AbstractLevelBuilder {
    private AtomicBoolean invalid = new AtomicBoolean();

    protected final void throwInvalid() {
        if (invalid.get()) {
            throw new IllegalStateException("outdated instance");
        }
    }

    public final void invalidate() {
        invalid.set(true);
    }

    public abstract Level nextLevel();

    public Level previousLevel() {
        throwInvalid();
        throw new UnsupportedOperationException("for debug pls refer DebugReverseDictionaryLevelBuilder");
    }

    private static AtomicReference<AbstractLevelBuilder> cached = new AtomicReference<>();

    public static AbstractLevelBuilder prepareLevelBuilder(Context ctx, Dictionary d, LevelSize size) {
        AbstractLevelBuilder lb = cached.get();

        if (lb instanceof DictionaryLevelBuilder) {
            DictionaryLevelBuilder builder = (DictionaryLevelBuilder) lb;
            if (builder != null && builder.context == ctx && builder.i.getDictionary() == d && builder.ls == size) {
                return lb;
            }
        }
        if (lb != null) {
            lb.invalidate();
        }
        lb = new DictionaryLevelBuilder(d, size, ctx);
        cached.set(lb);
        return lb;
    }

    public static AbstractLevelBuilder prepareLevelBuilder(Context ctx, Bundle savedInstanceState) {
        AbstractLevelBuilder lb = cached.get();
        if (lb != null) {
            lb.invalidate();
        }
        cached.set(null);
        lb = null;
        lb = LevelSerializer.instance.load(ctx, savedInstanceState);
        return lb;
    }
}
