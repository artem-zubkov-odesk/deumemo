package com.martiazoo.tort.deumemo.ui.level.start;

import android.graphics.Point;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.Utils;
import com.martiazoo.tort.deumemo.logic.level.Level;

import java.util.*;

public class StartLevelAnimator {
    private static final String TAG = "StartLevelAnimator";

    private final ActionBarActivity activity;
    private final Level level;

    public StartLevelAnimator(ActionBarActivity activity, Level level) {
        this.activity = activity;
        this.level = level;
    }

    public void animateLevelPopulation(final Runnable callback) {/*todo refactor me*/
        final ViewGroup gridview = (ViewGroup) activity.findViewById(R.id.gridview1);
        List<View> children = Utils.childrenAsList(gridview);
        List<Map.Entry<View, Integer>> wChildren = wrapToEntry(children);
        Log.d(TAG, "view matrix before sort:");
        printMatrix(wChildren, level.getPuzzleDimension());
        prioritizeByMainDiagonal(wChildren, level.getPuzzleDimension());
        Log.d(TAG, "view matrix after  prioritization:");
        printMatrix(wChildren, level.getPuzzleDimension());
        sortByValue(wChildren);
        Log.d(TAG, "view matrix after  sort by pd:");
        printMatrix(wChildren, level.getPuzzleDimension());

        List views = Lists.transform(wChildren, new Function<Map.Entry<View, Integer>, View>() {
            @Override
            public View apply(Map.Entry<View, Integer> input) {
                return input.getKey();
            }
        });
        level.levelController.animatePopulation(views, callback);
    }

    void prioritizeByMainDiagonal(List<Map.Entry<View, Integer>> src, Point asize) {
        /*todo for some reason not working if Y>X. Brutal hack here*/
        Point size = new Point(Math.max(asize.x, asize.y), Math.min(asize.x, asize.y));

        List<Integer> order = new ArrayList<>();
        int idx = 0;
        for (int diagonal = 0; diagonal < size.x; diagonal++) {
            idx = pointToIndex(diagonal, Math.min(diagonal, size.y - 1), size);
            order.add(idx);
            for (int delta = 1; delta <= diagonal; delta++) {
                if (diagonal <= size.y - 1) {
                    idx = pointToIndex(diagonal - delta, Math.min(diagonal, size.y - 1), size);
                    order.add(idx);
                }
                if (Math.min(diagonal, size.y - 1) - delta >= 0) {
                    idx = pointToIndex(diagonal - delta, Math.min(diagonal, size.y - 1), size);
                    order.add(idx);
                }
            }
        }
        if (order.size() != src.size()) {
            throw new IllegalStateException();
        }
        int c = 0;
        for (Map.Entry<View, Integer> entry : src) {
            idx = order.get(c);
            entry.setValue(idx);
            c++;
        }
    }

    int pointToIndex(int x, int y, Point size) {
        return x + y * size.x;
    }

    void printMatrix(List<Map.Entry<View, Integer>> list, Point size) {
        if (list == null || size == null || list.size() != size.x * size.y) {
            throw new IllegalArgumentException();
        }
        StringBuilder sb = new StringBuilder("printMatrix: object[rfr order]\n");
        int c = 0;
        for (Map.Entry<?, Integer> o : list) {
            if (c % size.x == 0) {
                sb.append("\n");
                sb.append("mx: ");
            }
            sb.append(String.format("%10.10s[%3.3s]", String.valueOf(o.getKey()), String.valueOf(o.getValue())));
            c++;
        }
        Log.d(TAG, sb.toString());
    }

    <T> List<Map.Entry<T, Integer>> wrapToEntry(List<T> src) {
        List<Map.Entry<T, Integer>> result = new ArrayList<>();
        int c = 0;
        for (T t : src) {
            result.add(new AbstractMap.SimpleEntry<T, Integer>(t, c));
            c++;
        }
        return result;
    }

    <T> void sortByValue(List<Map.Entry<T, Integer>> src) {
        Collections.sort(src, BY_VALUE_COMPARATOR);
    }

    private static class ByValueComparator implements Comparator<Map.Entry<?, Integer>> {
        @Override
        public int compare(Map.Entry<?, Integer> lhs, Map.Entry<?, Integer> rhs) {
            if (lhs == null || rhs == null) {
                throw new IllegalArgumentException();
            }
            if (lhs.getValue() == null || rhs.getValue() == null) {
                throw new IllegalArgumentException();
            }
            return lhs.getValue().compareTo(rhs.getValue());
        }
    }

    private static final Comparator<Map.Entry<?, Integer>> BY_VALUE_COMPARATOR = new ByValueComparator();
}
