package com.martiazoo.tort.deumemo.ui.cardtable;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.common.base.Supplier;
import com.martiazoo.tort.deumemo.logic.level.AbstractLevelBuilder;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.logic.level.LevelSerializer;
import org.apache.commons.lang3.exception.ExceptionUtils;

public class SaveStateController {
    private static final String TAG = "SaveStateController";
    private Context context;
    public final Runnable shuffleLevelRunnable;
    public final Supplier<Level> levelSupplier;

    private AbstractLevelBuilder restoredLevelBuilder;
    boolean isReturned = false;

    public SaveStateController(Context context, Runnable shuffleLevelRunnable, Supplier<Level> levelSupplier) {
        this.context = context;
        this.shuffleLevelRunnable = shuffleLevelRunnable;
        this.levelSupplier = levelSupplier;
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        restoredLevelBuilder = AbstractLevelBuilder.prepareLevelBuilder(context, savedInstanceState);
        if (restoredLevelBuilder != null) {
            isReturned = false;
            shuffleLevelRunnable.run();
        }
        Log.d(TAG, "onRestoreInstanceState");
    }

    protected void onSaveInstanceState(Bundle savedInstanceState) {
        boolean doSave = checkSaveRequired();
        Level l;
        if (doSave) {
            l = levelSupplier.get();
            Log.d(TAG, "onSaveInstanceState");
        } else {
            l = restoreLevel(restoredLevelBuilder);
            Log.d(TAG, "onSaveInstanceState");
        }
        if (l != null) {
            LevelSerializer.instance.save(l, savedInstanceState);
        }
        Log.d(TAG, "onSaveInstanceState");
    }

    private boolean checkSaveRequired() {
        Exception e = new Exception("checkSaveRequired marker");
        String s = ExceptionUtils.getStackTrace(e);
        boolean b = s.contains("ActivityThread.handleStopActivity");
        return b;
    }

    @Nullable
    public Level nextLevel() {
        if (isReturned) {
            return null;
        }
        isReturned = true;
        Level l = restoreLevel(restoredLevelBuilder);
        return l;
    }

    private Level restoreLevel(AbstractLevelBuilder restoredLevelBuilder) {
        if (restoredLevelBuilder != null) {
            Level l = restoredLevelBuilder.nextLevel();
            if (l != null) {
                return l;
            }
        }
        return null;
    }
}
