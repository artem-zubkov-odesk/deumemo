package com.martiazoo.tort.deumemo.logic.autoplay;

import android.support.annotation.Nullable;
import android.util.Log;
import com.martiazoo.tort.deumemo.logic.card.CardState;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;
import com.martiazoo.tort.deumemo.util.threading.Heartbeat;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.ArrayList;
import java.util.List;

public class LevelAutoPlayer extends AbstractLevelAutoPlayer {
    private static final String TAG = "LevelAutoPlayer";

    public static LevelAutoPlayer instance = new LevelAutoPlayer();

    static {
        Heartbeat.get().register(instance);
    }

    private GFElement a;
    private GFElement b;
    private GFElement single;
    private final Runnable initAB = new Runnable() {
        @Override
        public void run() {
            throwWrongThread();
            if (isStopped.get()) {
                imDone(this);
                return;
            }
            single = nextSingleOpened();
            if (single != null) {
                imDone(this);
                return;
            }
            ImmutablePair<GFElement, GFElement> p = nextMatch();
            if (p == null) {
                imDone(this);
                return;
            }
            a = p.getLeft();
            b = p.getRight();
            imDone(this);
        }
    };
    private final Runnable openSingle = new Runnable() {
        @Override
        public void run() {
            throwWrongThread();
            if (isStopped.get()) {
                imDone(this);
                return;
            }
            GFElement target = single;
            single = null;
            if (target == null) {
                imDone(this);
                return;
            }
            cardOpenPredicate.evaluate(target);
            imDone(this);
        }
    };

    private final Runnable openA = new Runnable() {
        @Override
        public void run() {
            throwWrongThread();
            if (isStopped.get()) {
                imDone(this);
                return;
            }
            GFElement target = a;
            a = null;
            if (target == null) {
                imDone(this);
                return;
            }
            cardOpenPredicate.evaluate(target);
            imDone(this);
        }
    };
    private final Runnable delayMiddle = new Runnable() {
        @Override
        public void run() {
            throwWrongThread();
            if (isStopped.get()) {
                imDone(this);
                return;
            }
            imDone(this);
        }
    };
    private final Runnable openB = new Runnable() {
        @Override
        public void run() {
            throwWrongThread();
            if (isStopped.get()) {
                imDone(this);
                return;
            }
            GFElement target = b;
            b = null;
            if (target == null) {
                imDone(this);
                return;
            }
            cardOpenPredicate.evaluate(target);
            imDone(this);
        }
    };
    private final Runnable delayEnd = new Runnable() {
        @Override
        public void run() {
            throwWrongThread();
            if (isStopped.get()) {
                imDone(this);
                return;
            }
            imDone(this);
        }
    };
    private final Runnable shuffleLevelIfDone = new Runnable() {
        @Override
        public void run() {
            throwWrongThread();
            if (isStopped.get()) {
                imDone(this);
                return;
            }
            boolean isCompleteScreen = isLevelDoneDialogOnScreenPredicate.evaluate(null);
            if (!isCompleteScreen) {
                imDone(this);
                return;
            }
            shuffleLevelRunnable.run();
            imDone(this);
        }
    };

    private void imDoneSingleCardFlow(Runnable r) {
        if (isStopped.get()) {
            return;
        }
        if (r == initAB) {
            if (single == null) {
                return;
            }
            register(openSingle, 100, "LevelAutoPlayer openSingle");
            return;
        }
        if (r == openSingle) {
            register(delayEnd, 100, "LevelAutoPlayer delayEnd");
            return;
        }
    }

    private void imDoneCardTableFlow(Runnable r) {
        if (isStopped.get()) {
            return;
        }
        if (r == initAB) {
            if (a == null) {
                return;
            }
            register(openA, 100, "LevelAutoPlayer openA");
            return;
        }
        if (r == openA) {
            register(delayMiddle, 3000, "LevelAutoPlayer delayMiddle");
            return;
        }
        if (r == delayMiddle) {
            if (b == null) {
                return;
            }
            register(openB, 100, "LevelAutoPlayer openB");
            return;
        }
        if (r == openB) {
            register(delayEnd, 100, "LevelAutoPlayer delayEnd");
            return;
        }
        if (r == delayEnd) {
            register(initAB, 4000, "LevelAutoPlayer initAB");
            return;
        }
    }

    private void imDoneCompleteScreenFlow(Runnable r) {
        if (isStopped.get()) {
            return;
        }
        if (r == initAB) {
            if (a != null) {
                return;
            }
            if (b != null) {
                return;
            }
            if (single != null) {
                return;
            }
            register(shuffleLevelIfDone, 10_000, "LevelAutoPlayer shuffleLevelIfDone");
            return;
        }
        /*will be called automatically via start()*/
//        if (r == shuffleLevelIfDone) {
//            register(initAB, 5_000, "LevelAutoPlayer initAB imDoneCompleteScreenFlow");
//        }
    }

    private void imDone(Runnable r) {
        /*smg already opened flow*/
        imDoneSingleCardFlow(r);
        /*main flow*/
        imDoneCardTableFlow(r);
        /*main flow*/
        imDoneCompleteScreenFlow(r);
    }

    @Override
    public void heartbeat() {
        super.heartbeat();
        /*check if nothing to pair - generate next level after timeout then*/
        if (isStopped.get()) {
            return;
        }
        boolean isCompleteScreen = isLevelDoneDialogOnScreenPredicate.evaluate(null);
        if (!isCompleteScreen) {
            return;
        }
        register(shuffleLevelIfDone, 10_000, "LevelAutoPlayer shuffleLevelIfDone");
        return;
    }

    @Override
    public void start() {
        if (!isScreenOn()) {
            Log.d(TAG, "avoiding start as screen is off");
            return;
        }

        super.start();
        /*timeout and then pairing*/
/*todo big duration is to complete population animation. Brutal hack*/
        register(initAB, 5000, "LevelAutoPlayer start thread shift");
    }

    @Nullable
    public ImmutablePair<GFElement, GFElement> nextMatch() {
        List<GFElement> available = new ArrayList<>(level.getElements());
        CollectionUtils.filter(available, new Predicate<GFElement>() {
            @Override
            public boolean evaluate(GFElement input) {
                return input.getCardState() == CardState.BACK;
            }
        });
        if (available.isEmpty()) {
            return null;
        }
        GFElement left = available.get((int) (Math.random() * available.size()));
        if (left == null) {
            return null;
        }
        GFElement right = GFElement.findPair(left, level.getElements());
        if (right == null) {
            return null;
        }
        Log.d(TAG, String.format("returning pair: \nleft [%s] \nright[%s]", left, right));
        return new ImmutablePair<>(left, right);
    }

    @Nullable
    public GFElement nextSingleOpened() {
        List<GFElement> available = new ArrayList<>(level.getElements());
        CollectionUtils.filter(available, new Predicate<GFElement>() {
            @Override
            public boolean evaluate(GFElement input) {
                return input.getCardState() == CardState.TOP;
            }
        });
        if (available.isEmpty()) {
            return null;
        }
        if (available.size() == 2) {
            return null;
        }
        GFElement left = available.get(0);

        GFElement right = null;
        PairType p = left.getPair();
        for (GFElement element : level.getElements()) {
            if (element != left && element.getPair() == p) {
                right = element;
                break;
            }
        }
        if (left == null || right == null) {
            return null;
        }

        Log.d(TAG, String.format("returning nextSingleOpened: \nleft [%s] \nright[%s]", left, right));
        return right;
    }

    @Override
    public AbstractLevelAutoPlayer clear() {
        super.clear();
        a = null;
        b = null;
        single = null;
        return this;
    }
}
