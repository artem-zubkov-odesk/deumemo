package com.martiazoo.tort.deumemo.ui.card;

import android.app.Activity;
import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.Log;
import android.view.Display;
import com.martiazoo.tort.deumemo.R;

public class CardBitmapRenderer {
    private static CardBitmapRenderer instance;

    public static CardBitmapRenderer getInstance(Activity activity) {
        if (instance == null || instance.ctx != activity) {
            instance = new CardBitmapRenderer(activity);
        }
        return instance;
    }

    private static final String TAG = "CardBitmapRenderer";
    private static double MAGIC_INNER_FRAME_INTENT = 0.91;
    private double renderingSampleHeight = 216;
    public final Context ctx;
    private int w;
    private int h;
    private Bitmap cachedBack;
    private Bitmap cachedFront;
    private Paint textPaint;
    private boolean doInvalidate;


    public CardBitmapRenderer(Activity activity) {
        this.ctx = activity;
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point displaySize = new Point(100, 100);
        display.getSize(displaySize);
        renderingSampleHeight = ((double) displaySize.y) * 0.8;
    }

    public void notifyTextPaint(Paint textPaint) {
        if (this.textPaint != null && this.textPaint != textPaint) {
            throw new UnsupportedOperationException("Cannot reassign different textPaint at runtime");
        }
        this.textPaint = textPaint;
    }

    public void notifyHeight(int h) {
        if (this.h == h) {
            return;
        }
        this.h = h;
        doInvalidate = true;
    }

    public void notifyWidth(int w) {
        if (this.w == w) {
            return;
        }
        this.w = w;
        doInvalidate = true;
    }

    public Bitmap getBackBitmap() {
        throwInitialization();
        return cachedBack;
    }

    public Bitmap getFrontBitmap() {
        throwInitialization();
        return cachedFront;
    }

    public void tryInvalidate() {
        if (doInvalidate) {
            doInvalidate = false;
            invalidate();
        }
    }

    private void invalidate() {
        Log.e(TAG, String.format("invalidating both back and front bitmaps: w[%d] h[%d]", w, h));
        if (cachedBack != null) {
            cachedBack.recycle();
            cachedBack = null;
        }
        if (cachedFront != null) {
            cachedFront.recycle();
            cachedFront = null;
        }

        double sideRatio = ((double) w) / h;
        Bitmap unscaledBack = getBackBitmap(
                (int) (renderingSampleHeight * sideRatio)
                , (int) (renderingSampleHeight));
        cachedBack = scale(unscaledBack, w, h);

        Bitmap unscaledFront = getFrontBitmap(
                (int) (renderingSampleHeight * sideRatio)
                , (int) (renderingSampleHeight));
        cachedFront = scale(unscaledFront, w, h);
        unscaledBack.recycle();
        unscaledFront.recycle();
    }

    private void throwInitialization() {
//        if (counter % 2 == 0) {
//            throw new IllegalStateException("counter in the middle of dimension set");
//        }
        if (w == 0 || h == 0) {
            throw new IllegalStateException("please set dimensions first");
        }
        if (textPaint == null) {
            throw new IllegalStateException("please set textPaint first");
        }
    }

    public int getXIntent() {
        return (int) (w * (1 - MAGIC_INNER_FRAME_INTENT));
    }

    public int getYIntent() {
        return (int) (h * (1 - MAGIC_INNER_FRAME_INTENT));
    }

    private Bitmap getFrontBitmap(int w, int h) {
        Bitmap output_bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output_bitmap);
        fillColor(canvas);
        drawOuterBorder(canvas);
        drawInnerBorder(canvas);
        return output_bitmap;
    }

    private Bitmap getBackBitmap(int w, int h) {
        Bitmap output_bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output_bitmap);
        fillColor(canvas);
        drawOuterBorder(canvas);
        drawPattern(canvas);
        drawInnerBorder(canvas);
        drawQuestionSign(canvas);
        return output_bitmap;
    }

    private void drawOuterBorder(Canvas canvas) {
        Bitmap b = get_ninepatch(R.drawable.lukash_frame, canvas.getWidth(), canvas.getHeight(), ctx);
        int l = canvas.getWidth() - b.getWidth();
        l /= 2;
        int t = canvas.getHeight() - b.getHeight();
        t /= 2;
        canvas.drawBitmap(b, l, t, textPaint);
        b.recycle();
    }

    private void drawInnerBorder(Canvas canvas) {
        Bitmap b = get_ninepatch(R.drawable.lukash_insideframe
                , (int) (canvas.getWidth() * MAGIC_INNER_FRAME_INTENT)
                , (int) (canvas.getHeight() * MAGIC_INNER_FRAME_INTENT), ctx);
        int l = canvas.getWidth() - b.getWidth();
        l /= 2;
        int t = canvas.getHeight() - b.getHeight();
        t /= 2;
        canvas.drawBitmap(b, l, t, textPaint);
    }

    private void drawPattern(Canvas canvas) {
        BitmapDrawable background = new BitmapDrawable(BitmapFactory.decodeResource(
                ctx.getResources(), R.drawable.lukash_back_card_fill_pattern));
        int l = (int) (canvas.getWidth() * (1 - MAGIC_INNER_FRAME_INTENT));
        int t = (int) (canvas.getHeight() * (1 - MAGIC_INNER_FRAME_INTENT));
        l /= 2;
        t /= 2;
        int r = canvas.getWidth() - l;
        int b = canvas.getHeight() - t;
        background.setBounds(l, t, r, b);
        background.setTileModeXY(Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
        background.draw(canvas);
    }

    private void drawQuestionSign(Canvas canvas) {
        Bitmap b = BitmapFactory.decodeResource(ctx.getResources(), R.drawable.lukash_question_sign);
        {
            double factor = ((double) canvas.getHeight() / 2) / b.getHeight();
            Bitmap b2 = scale(b, (int) (b.getWidth() * factor), (int) (b.getHeight() * factor));
            b.recycle();
            b = null;
            b = b2;
        }
        int l = canvas.getWidth() - b.getWidth();
        l /= 2;
        int t = canvas.getHeight() - b.getHeight();
        t /= 2;
        canvas.drawBitmap(b, l, t, textPaint);
        b.recycle();
    }

    private static Bitmap flipMirrorBitmap(Bitmap source) {
        Matrix matrix = new Matrix();
        matrix.preScale(-1, 1);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    private static void fillColor(Canvas canvas) {
        canvas.drawColor(Color.TRANSPARENT);
    }

    public static Bitmap scale(Bitmap bm, int newWidth, int newHeight) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
//        bm.recycle();
        return resizedBitmap;
    }

    public static Bitmap get_ninepatch(int id, int x, int y, Context context) {
        // id is a resource id for a valid ninepatch

        Bitmap bitmap = BitmapFactory.decodeResource(
                context.getResources(), id);

        byte[] chunk = bitmap.getNinePatchChunk();
        NinePatchDrawable np_drawable = new NinePatchDrawable(bitmap,
                chunk, new Rect(), null);
        np_drawable.setBounds(0, 0, x, y);

        Bitmap output_bitmap = Bitmap.createBitmap(x, y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output_bitmap);
        np_drawable.draw(canvas);

        return output_bitmap;
    }
}
