package com.martiazoo.tort.deumemo.logic.dictionary;

import android.content.Context;
import com.martiazoo.tort.deumemo.logic.xml.generated.DictionaryType;

import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class PairCollectionLoader extends DictionaryFileLoader {
    private AtomicBoolean loadedAll = new AtomicBoolean();
    private Map<String, PairCollection> all = new HashMap<>();

    public PairCollectionLoader(Context ctx, String subDirectory) {
        super(ctx, subDirectory);
    }

    public Map<String, String> getFilenameDisplayName() {
        loadAll();
        Map<String, String> map = new LinkedHashMap<>();
        for (PairCollection pc : all.values()) {
            map.put(pc.dictionary_filename, pc.toDisplayName());
        }
        return map;
    }

    String getDefaultFilename() {
        return listXmlFiles().get(0);
    }

    PairCollection getPairCollection(String file) {
        loadSingle(file);
        PairCollection pc = all.get(file);
        return pc;
    }

    private void loadAll() {
        if (loadedAll.get()) {
            return;
        }
        List<String> files = listXmlFiles();
        if (files == null || files.size() == 0) {
            throw new IllegalArgumentException("No xml files found in dictionary folder " + subDirectory);
        }
        for (String file : files) {
            loadSingle(file);
        }
        loadedAll.set(true);
    }

    private void loadSingle(String file) {
        if (loadedAll.get()) {
            return;
        }
        if (all.containsKey(file)) {
            return;
        }
        DictionaryType vt = load(file);/*todo lazy load here*/
        if (vt == null) {
            return;
        }
        PairCollection pc = PairCollection.toPairCollection(vt);
        all.put(file, pc);
    }
}
