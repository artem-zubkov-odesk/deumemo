package com.martiazoo.tort.deumemo.logic.scope;

import java.util.HashMap;
import java.util.Map;

public class ValueHolder {

    private Map<String, Object> values = new HashMap<>();

    public Map<String, Object> getValues() {
        return values;
    }
}
