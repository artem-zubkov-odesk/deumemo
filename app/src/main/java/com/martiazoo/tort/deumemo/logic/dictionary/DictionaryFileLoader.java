package com.martiazoo.tort.deumemo.logic.dictionary;

import android.content.Context;
import android.content.res.AssetManager;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.martiazoo.tort.deumemo.logic.xml.generated.DictionaryType;
import org.simpleframework.xml.core.Persister;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class DictionaryFileLoader {
    final Context ctx;
    final String subDirectory;
    List<String> xmls;
    private boolean isXmlsLoaded;

    public DictionaryFileLoader(Context ctx, String subDirectory) {
        this.ctx = ctx;
        this.subDirectory = subDirectory;
    }

    /**
     * only filename with extension
     *
     * @param filename buro.xml
     */
    protected DictionaryType load(String filename) {
        try {
            InputStream is = ctx.getAssets().open(subDirectory + System.getProperty("file.separator") + filename);
            Persister persister = new Persister();
            DictionaryType vt = persister.read(DictionaryType.class, is);
            return vt;
        } catch (FileNotFoundException e) {
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    protected List<String> listXmlFiles() {
        if (isXmlsLoaded) {
            return xmls;
        }
        List<String> files = listFiles(subDirectory);
        Collection<String> xmlFiles = filterFiles(files, ".xml");
        xmls = new ArrayList<>(xmlFiles);
        isXmlsLoaded = true;
        return xmls;
    }

    protected List<String> listFiles(String dirFrom) {
        try {
            AssetManager am = ctx.getResources().getAssets();
            String fileList[] = new String[0];
            fileList = am.list(dirFrom);
            return Arrays.asList(fileList);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    protected Collection<String> filterFiles(Collection<String> names, final String dotExtension) {
        return Collections2.filter(names, new Predicate<String>() {
            @Override
            public boolean apply(String input) {
                return input != null && input.endsWith(dotExtension);
            }
        });
    }
}
