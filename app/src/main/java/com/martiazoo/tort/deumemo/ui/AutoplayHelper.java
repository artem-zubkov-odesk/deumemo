package com.martiazoo.tort.deumemo.ui;

import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.Utils;
import com.martiazoo.tort.deumemo.logic.autoplay.AbstractLevelAutoPlayer;
import com.martiazoo.tort.deumemo.logic.autoplay.LevelAutoPlayer;

import java.util.concurrent.atomic.AtomicBoolean;

public class AutoplayHelper {
    private ActionBarActivity ctx;
    private final AtomicBoolean animationInProgress = new AtomicBoolean();
    private LevelAutoPlayer levelAutoPlayer = LevelAutoPlayer.instance;
    public final AbstractLevelAutoPlayer.Listener autoPlayerListener = new AbstractLevelAutoPlayer.Listener() {
        @Override
        public void started() {
            animateActivate();
        }

        @Override
        public void stopped() {
            animateDeactivate();
        }
    };

    public AutoplayHelper(ActionBarActivity parent) {
        this.ctx = parent;
    }

    public void onCreate(Bundle savedInstanceState) {

        updateMenuHintImageSize();
    }

    public void updateMenuHintImageSize() {
        final ImageView iv = (ImageView) ctx.findViewById(R.id.menuAutoplayButton);
        Point size = topMenuHintButtonSize();
        ViewGroup.LayoutParams params = iv.getLayoutParams();
        params.width = size.x;
        params.height = size.y;
        iv.setLayoutParams(params);
    }

    private Point topMenuHintButtonSize() {
        Point size = new Point();
        size.x = (int) (Utils.getActionBarHeight(ctx) * 0.7d);

        Drawable drawable = ctx.getResources().getDrawable(R.drawable.magic_wand);
        double factor = ((double) drawable.getMinimumHeight()) / drawable.getMinimumWidth();
        size.y = (int) (factor * size.x);
        return size;
    }

    public void menuAutoplayButtonClicked() {
        if (levelAutoPlayer.isStarted()) {
            levelAutoPlayer.stop();
            return;
        }
        levelAutoPlayer.start();
    }

    public boolean isStarted() {
        return levelAutoPlayer.isStarted();
    }

    private void animateActivate() {
        if (animationInProgress.get()) {
            return;
        }
        animationInProgress.set(true);
        ImageView imageView = (ImageView) ctx.findViewById(R.id.menuAutoplayButton);
        if (imageView == null) {
            return;
        }

        Animation pulse = AnimationUtils.loadAnimation(ctx, R.anim.start_magic_wand);
        imageView.startAnimation(pulse);
    }


    private void animateDeactivate() {
        if (!animationInProgress.get()) {
            return;
        }
        animationInProgress.set(false);
        ImageView imageView = (ImageView) ctx.findViewById(R.id.menuAutoplayButton);
        if (imageView == null) {
            return;
        }
        Animation pulse = AnimationUtils.loadAnimation(ctx, R.anim.stop_magic_wand);
        imageView.startAnimation(pulse);
    }
}
