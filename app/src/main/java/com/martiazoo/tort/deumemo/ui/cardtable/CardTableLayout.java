package com.martiazoo.tort.deumemo.ui.cardtable;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

public class CardTableLayout extends ViewGroup {
    private static final String TAG = "CardTableLayout";

    int xPcs = 20;
    int yPcs = 20;
    int ySeparator = 0x123;
    int xSeparator = 0x123;
    private Point elementSize = new Point();
    private View.OnTouchListener customOnTouchListener = new OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return false;
        }
    };

    public CardTableLayout(Context context) {
        super(context);
    }

    public CardTableLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setOnTouchListener(OnTouchListener l) {
        setCustomOnTouchListener(l);
    }

    public void setCustomOnTouchListener(OnTouchListener customOnTouchListener) {
        this.customOnTouchListener = customOnTouchListener;
    }

    public void setYPcs(int yPcs) {
        this.yPcs = yPcs;
    }

    public void setXPcs(int xPcs) {
        this.xPcs = xPcs;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        boolean isProcessed = customOnTouchListener.onTouch(this, event);
        Log.d(TAG, "onTouchEvent: isProcessed: " + isProcessed);
//        if(!isProcessed){
//            super.onTouchEvent(event);
//        }
        return true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        super.onInterceptTouchEvent(event);
        boolean isProcessed = customOnTouchListener.onTouch(this, event);
        Log.d(TAG, "onInterceptTouchEvent: isProcessed: " + isProcessed);
        return false;
    }

    @Override
    protected LayoutParams generateDefaultLayoutParams() {
        return new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    }

    @Override
    protected LayoutParams generateLayoutParams(LayoutParams p) {
        return new MarginLayoutParams(p);
    }

    @Override
    public LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new MarginLayoutParams(getContext(), attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // breakdown specs
//        final int mw = MeasureSpec.getMode(widthMeasureSpec);
//        final int mh = MeasureSpec.getMode(heightMeasureSpec);
        int w = MeasureSpec.getSize(widthMeasureSpec);
        int h = MeasureSpec.getSize(heightMeasureSpec);

        /*hack to avoid one more unnecessary spacing on the bottom*/
//        h += ySeparator;

//        dynamic separators - 1/3 card height, 1/2 card width
        {
            double xSeparatorFactor = (double) 1 / 5;
            double ySeparatorFactor = (double) 1 / 3;

            double xTotalPcs = xPcs + xPcs * xSeparatorFactor;
            double yTotalPcs = yPcs + yPcs * ySeparatorFactor;

            double xElemSize = w / xTotalPcs;
            double yElemSize = h / yTotalPcs;

            xSeparator = (int) (xElemSize * xSeparatorFactor);
            ySeparator = (int) (yElemSize * ySeparatorFactor);
        }

        elementSize.x = w / xPcs;
        elementSize.x -= xSeparator;
        elementSize.y = h / yPcs;
        elementSize.y -= ySeparator;

        if (elementSize.y > elementSize.x) {
            elementSize.y = elementSize.x;
        }

//        elementSize.x += 20;
//        elementSize.y += 20;

        Log.d(TAG, "gridview.getMeasuredWidth: " + w);
        Log.d(TAG, "gridview.getMeasuredHeight: " + h);
        Log.d(TAG, "elementSize: " + elementSize);

        setMeasuredDimension(w, h);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int halfLeft = xSeparator / 2;
        int halfTop = ySeparator / 2;
//        {
//            // adjust for our padding
//            final int pl = getPaddingLeft();
//            final int pt = getPaddingTop();
//            final int pr = getPaddingRight();
//            final int pb = getPaddingBottom();
//
//            // allocate any extra spare space evenly
//            l = pl + (r - pr - l - pl - s) / 2;
//            t = pt + (b - pb - t - pb - s) / 2;
//        }

        for (int y = 0; y < yPcs; y++) {
            for (int x = 0; x < xPcs; x++) {
                View child = getChildAt(y * xPcs + x);
                // optimization: we are moving through the children in order
                // when we hit null, there are no more children to layout so return
                if (child == null) return;
                // get the child's layout parameters so that we can honour their margins
//MarginLayoutParams lps = (MarginLayoutParams) child.getLayoutParams();
                // we don't support gravity, so the arithmetic is simplified
//                child.layout(
//                        l + (s * x) / size + lps.leftMargin,
//                        t + (s * y) / size + lps.topMargin,
//                        l + (s * (x + 1)) / size - lps.rightMargin,
//                        t + (s * (y + 1)) / size - lps.bottomMargin
//                );
                int currentL = x * (elementSize.x + xSeparator);
                int currentT = y * (elementSize.y + ySeparator);
                currentL += halfLeft;
                currentT += halfTop;
                ViewGroup.LayoutParams lp = new AbsListView.LayoutParams(elementSize.x, elementSize.y);
                child.setLayoutParams(lp);
                child.layout(
                        currentL,
                        currentT,
                        currentL + elementSize.x,
                        currentT + elementSize.y
                );
            }
        }
    }
}