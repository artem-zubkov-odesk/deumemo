package com.martiazoo.tort.deumemo.ui.level.done;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.logic.level.done.LevelCompleteInfo;
import com.martiazoo.tort.deumemo.ui.cardtable.CardTableActivity;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class LevelDoneStaticHelper {
    private static final String TAG = "LevelDoneStaticHelper";
    private CardTableActivity ctx;
    private ViewGroup viewGroup;

    public LevelDoneStaticHelper(CardTableActivity parent) {
        this.ctx = parent;
    }

    void fill(LevelCompleteInfo levelCompleteInfo) {
//        hideWordListInternal();
        List<LevelCompleteInfo.Element> elements = levelCompleteInfo.toElements();
        populateRows(elements);

//        ImageView imageView = (ImageView) ctx.findViewById(R.id.level_complete_restart_image);
//        Animation pulse = AnimationUtils.loadAnimation(ctx, R.anim.pulse);
//        imageView.startAnimation(pulse);
    }

    public void onCreate(Bundle savedInstanceState) {
        viewGroup = (ViewGroup) ctx.findViewById(R.id.level_done_static_layout);

        AdView mAdView = (AdView) ctx.findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)        // All emulators
//                .addTestDevice("AC98C820A50B4AD8A2106EDE96FB87D4")  // An example device ID
                .build();
        mAdView.loadAd(adRequest);

//        fill(LevelCompleteInfo.nextDummy());

        /*adjust center layout size*/
        ViewGroup layout;
        RelativeLayout.LayoutParams lp;
        layout = (ViewGroup) ctx.findViewById(R.id.level_done_center_layout);
        lp = (RelativeLayout.LayoutParams) layout.getLayoutParams();
        lp.setMargins(0, ctx.getActionBarFullHeight(), 0, /*ctx.displaySize.y - */ctx.getActionBarFullHeight());
        layout.setLayoutParams(lp);

        layout = (ViewGroup) ctx.findViewById(R.id.level_done_top_layout);
        lp = (RelativeLayout.LayoutParams) layout.getLayoutParams();
        lp.setMargins(0, 0, 0, ctx.displaySize.y - ctx.getActionBarFullHeight());
        layout.setLayoutParams(lp);

        layout = (ViewGroup) ctx.findViewById(R.id.level_done_bottom_layout);
        lp = (RelativeLayout.LayoutParams) layout.getLayoutParams();
        lp.setMargins(0, ctx.displaySize.y - ctx.getActionBarFullHeight(), 0, 0);
        layout.setLayoutParams(lp);

    }

    private void populateRows(List<LevelCompleteInfo.Element> elements) {
        int i = 0;
        for (LevelCompleteInfo.Element element : elements) {
            int text_id = ctx.getResources().getIdentifier("TextView_" + i, "id", ctx.getPackageName());
            ViewGroup textRow = (ViewGroup) viewGroup.findViewById(text_id);
            int image_id = ctx.getResources().getIdentifier("ImageView_" + i, "id", ctx.getPackageName());
            ViewGroup imageRow = (ViewGroup) viewGroup.findViewById(image_id);
            updateRow(textRow, imageRow, element.text, element.drawableResourceId);
            i++;
        }

        RelativeLayout.LayoutParams lp;
        ImageView imageView = (ImageView) ctx.findViewById(R.id.level_complete_restart_image);
        if (imageView != null) {
            int left = 0;
            int top = 0;
            int size = 0;
            int location[] = new int[2];
            View view = (View) ctx.findViewById(R.id.TextView_1);
            size += view.getHeight();
            view = (View) ctx.findViewById(R.id.placeholderB2);
            size += view.getHeight();
            view.getLocationOnScreen(location);
            top = location[1] - size / 2 + view.getHeight() / 2;
            view = (View) ctx.findViewById(R.id.ld_placeholder0);
            view.getLocationOnScreen(location);
            left = location[0] + view.getWidth() / 2 - size / 2;
//            ld_placeholder0
//            left = 550;
//            top = 550;
//            size = 50;
            lp = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
            lp.setMargins(left, top, left + size, top + size);
            imageView.setLayoutParams(lp);
            Animation pulse = AnimationUtils.loadAnimation(ctx, R.anim.pulse);
            imageView.startAnimation(pulse);

            imageView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ctx.forceNextLevel(null);
                }
            });
        }
    }

    private void updateRow(ViewGroup textRow, ViewGroup imageRow, String text, int drawableResourceId) {
        if (textRow != null) {
            TextView textView = (TextView) textRow.findViewById(R.id.level_complete_element_text);
            textView.setText(text);
        }
        if (imageRow != null) {
            ImageView imageView = (ImageView) imageRow.findViewById(R.id.level_complete_element_image);
            imageView.setImageResource(drawableResourceId);

//            Animation pulse = AnimationUtils.loadAnimation(ctx, R.anim.pulse);
//            imageView.startAnimation(pulse);
        }
    }

    public void _dbgShowComplete(LevelCompleteInfo levelCompleteInfo) {
        Toast.makeText(ctx, "_dbgShowComplete clc", Toast.LENGTH_SHORT).show();
        View view = ctx.findViewById(R.id.level_done_static_layout);
        int visibility = view.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE;
        view.setVisibility(visibility);
        this.fill(levelCompleteInfo);
        view.invalidate();
    }

    private AtomicBoolean isShown = new AtomicBoolean();

    public boolean isShown() {
        return isShown.get();
    }

    public void showLevelCompleteDialog(LevelCompleteInfo levelCompleteInfo) {
        isShown.set(true);
        this.fill(levelCompleteInfo);
        View view = ctx.findViewById(R.id.level_done_static_layout);
        view.setVisibility(View.VISIBLE);
        view.invalidate();
    }

    public void hideLevelCompleteDialog() {
        isShown.set(false);
        View view = ctx.findViewById(R.id.level_done_static_layout);
        view.setVisibility(View.INVISIBLE);
        view.invalidate();
    }
}
