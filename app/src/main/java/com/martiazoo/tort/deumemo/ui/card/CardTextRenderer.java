package com.martiazoo.tort.deumemo.ui.card;


import android.content.Context;
import android.graphics.*;
import android.util.Log;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import java.util.concurrent.atomic.AtomicReference;
import static com.martiazoo.tort.deumemo.Utils.splitWords;

public class CardTextRenderer {
    private static final String TAG = "CardTextRenderer";
    private static AtomicReference<CardTextRenderer> instance = new AtomicReference<>();

    public static CardTextRenderer getInstance(Context activity) {
        if (instance.get() == null) {
            instance.compareAndSet(null, new CardTextRenderer(activity));
        }
        return instance.get();
    }

    public final Context ctx;
    private int cardH;

    private int cardW;
    public Paint textPaint;

    private CardTextRenderer(Context activity) {
        this.ctx = activity;
        defaultTextPaint();
    }

    private void defaultTextPaint() {
        Typeface plain = Typeface.createFromAsset(ctx.getAssets(), "fonts/aleo-regular-webfont.ttf");
        textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(Color.BLACK);
        textPaint.setStrokeWidth(1);
        textPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        textPaint.setTextSize(3);
        textPaint.setTypeface(Typeface.create(plain, Typeface.NORMAL));
    }

    public void notifyCardHeight(int h) {
        this.cardH = h;
    }

    public void notifyCardWidth(int w) {
        this.cardW = w;
    }

    private static void normalizeRect(Rect r) {
        Log.d(TAG, "before normalizeRect: " + r);
        r.right += Math.abs(r.left);
        r.left = 0;

        r.bottom += Math.abs(r.top);
        r.top = 0;
        Log.d(TAG, "after normalizeRect: " + r);
    }


    /**
     * @return abstract bounds for text with abstract size. Need to be scaled afterwards
     */
    private Rect[] abstractBounds(String[] lines, Paint textPaint) {

        String text;
        Rect r = new Rect();
        Rect[] bounds = new Rect[lines.length];

        for (int i = 0; i < lines.length; i++) {
            text = lines[i];
            textPaint.getTextBounds(text, 0, text.length(), r);
            bounds[i] = new Rect(r);
            normalizeRect(bounds[i]);
        }
        return bounds;
    }

    /**
     * @return abstract bounds for text with abstract size. Need to be scaled afterwards
     */
    private Point abstractBitmapSize(Rect[] bounds, Paint.FontMetrics fm) {
        float fullHeight = Math.abs(fm.top) + Math.abs(fm.bottom);
        for (Rect bound : bounds) {
            bound.top = 0;
            bound.bottom = (int) fullHeight;
        }
        int sumH = (int) fullHeight * bounds.length;

        int maxW = 0;
        for (Rect bound : bounds) {
            maxW = Math.max(bound.width(), maxW);
        }
        return new Point(maxW, sumH);
    }

    public Bitmap getFrontBitmap(GFElement el) {

        float textSize = cardH;//calculateTextSize(lines);
        textPaint.setTextSize(textSize);
        Paint.FontMetrics fm = textPaint.getFontMetrics();

        String[] lines = splitWords(el.getString());
        Rect[] bounds = abstractBounds(lines, textPaint);
        Point bmpSize = abstractBitmapSize(bounds, fm);
        double factor;
        {
            double factorW = ((double) (cardW)) / bmpSize.x;
            double factorH = ((double) (cardH)) / bmpSize.y;
            factor = Math.min(factorW, factorH);
        }
        {
            /*synchronizing font side inside of card pair*/
            String[] lines2 = splitWords(!el.isA() ? el.getPair().getA() : el.getPair().getB());
            Rect[] bounds2 = abstractBounds(lines2, textPaint);
            Point bmpSize2 = abstractBitmapSize(bounds2, fm);
            double factor2;
            double factorW = ((double) (cardW)) / bmpSize2.x;
            double factorH = ((double) (cardH)) / bmpSize2.y;
            factor2 = Math.min(factorW, factorH);
            factor = Math.min(factor2, factor);
        }

        Bitmap output_bitmap = Bitmap.createBitmap(bmpSize.x, bmpSize.y, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output_bitmap);
//        canvas.drawColor(0x00);
//        canvas.drawColor(Color.RED);

        int i = 0;
        int prevH = (int) (bounds[0].height() / 2 - (fm.descent + fm.ascent) / 2);
        for (Rect bound : bounds) {
            String text = lines[i];
            int indentL = (bmpSize.x - bound.width()) / 2;
            canvas.drawText(text, indentL, prevH, textPaint);
            i++;
            prevH += bound.height();
        }


        int bmpW = (int) (factor * output_bitmap.getWidth());
        int bmpH = (int) (factor * output_bitmap.getHeight());

        output_bitmap = CardBitmapRenderer.scale(output_bitmap, bmpW, bmpH);
        return output_bitmap;
    }

    private static void debugDrawColoredLine(Canvas canvas, Paint tp, int c, int l, int t, int r, int b) {
        int oldColor = tp.getColor();
        float strokeW = tp.getStrokeWidth();
//        tp.setStrokeWidth(3);
        Paint.Style oldPs = tp.getStyle();
        tp.setStyle(Paint.Style.STROKE);
        tp.setColor(c);

        canvas.drawRect(l, t, r, b, tp);
        tp.setColor(oldColor);
        tp.setStrokeWidth(strokeW);
        tp.setStyle(oldPs);
    }

    private static void debugDrawColoredLine(Canvas canvas, Paint tp, int l, int t, int r, int b) {
        colorCntr++;
        colorCntr %= colors.length;
        debugDrawColoredLine(canvas, tp, colors[colorCntr], l, t, r, b);
    }

    private static void debugDrawColoredRect(Canvas canvas, Paint tp, int c, Rect r) {
        debugDrawColoredLine(canvas, tp, c, r.left, r.top, r.right, r.bottom);
    }

    static int[] colors = {Color.RED, Color.GREEN, Color.MAGENTA, Color.YELLOW};
    static int colorCntr = 0;

    private static void debugDrawColoredRect(Canvas canvas, Paint tp, Rect r) {
        colorCntr++;
        colorCntr %= colors.length;
        debugDrawColoredLine(canvas, tp, colors[colorCntr], r.left, r.top, r.right, r.bottom);
    }
}
