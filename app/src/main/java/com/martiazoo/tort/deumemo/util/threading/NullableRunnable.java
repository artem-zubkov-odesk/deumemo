package com.martiazoo.tort.deumemo.util.threading;

public class NullableRunnable implements Runnable {
    static public final Runnable NOT_NULL_RUNNABLE = new NullableRunnable();

    private NullableRunnable() {
    }

    @Override
    public void run() {
        /*do nothing here*/
    }

    static Runnable wrap(Runnable r) {
        return r == null ? NOT_NULL_RUNNABLE : r;
    }

    public static Runnable getFirst(Runnable... r) {
        if (r == null || r.length == 0) {
            return NOT_NULL_RUNNABLE;
        }
        return wrap(r[0]);
    }
}