package com.martiazoo.tort.deumemo.util.threading.forceable;

import android.util.Log;
import com.martiazoo.tort.deumemo.util.threading.OneTimeRunnable;

abstract class ForceableRunnable extends OneTimeRunnable {
    private static final String TAG = "ForceableRunnable";
    private final String message;

    public ForceableRunnable(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "ForceableRunnable " + System.identityHashCode(this) + " : " + message;
    }

    @Override
    public final void oneTimeRun() {
        Log.d(TAG, "performing of: " + this);
        perform();
    }

    abstract public void perform();
}
