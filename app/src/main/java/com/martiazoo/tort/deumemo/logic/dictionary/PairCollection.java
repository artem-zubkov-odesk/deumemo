package com.martiazoo.tort.deumemo.logic.dictionary;

import com.google.common.base.Preconditions;
import com.martiazoo.tort.deumemo.logic.xml.generated.DictionaryType;
import com.martiazoo.tort.deumemo.logic.xml.generated.EntryType;
import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

class PairCollection {
    public final String dictionary_filename;
    public final String dictionary_subdirectory;
    public final String dictionary_sourceUrl;
    public final String dictionary_theme_A;
    public final String dictionary_theme_B;
    public List<PairType> pairs;

    private PairCollection(final String dictionary_filename,
                           final String dictionary_subdirectory,
                           final String dictionary_sourceUrl,
                           final String dictionary_theme_A,
                           final String dictionary_theme_B,
                           List<PairType> pairs) {
        this.pairs = pairs;
        this.dictionary_theme_B = dictionary_theme_B;
        this.dictionary_theme_A = dictionary_theme_A;
        this.dictionary_sourceUrl = dictionary_sourceUrl;
        this.dictionary_subdirectory = dictionary_subdirectory;
        this.dictionary_filename = dictionary_filename;
    }

    public String toDisplayName() {
        if (StringUtils.isBlank(dictionary_theme_B)) {
            return dictionary_theme_A;
        }
        return dictionary_theme_A + " - " + dictionary_theme_B;
    }

    public static PairCollection toPairCollection(DictionaryType dt) {
        String dictionary_filename = findMetadata("dictionary_filename", dt.getMetadata());
        String dictionary_subdirectory = findMetadata("dictionary_subdirectory", dt.getMetadata());
        String dictionary_sourceUrl = findMetadata("dictionary_sourceUrl", dt.getMetadata());
        String dictionary_theme_A = findMetadata("dictionary_theme_A", dt.getMetadata());
        String dictionary_theme_B = findMetadata("dictionary_theme_B", dt.getMetadata());
        Preconditions.checkNotNull(dictionary_filename);
        Preconditions.checkNotNull(dictionary_subdirectory);
        Preconditions.checkNotNull(dictionary_sourceUrl);
        Preconditions.checkNotNull(dictionary_theme_A);
//        Preconditions.checkNotNull(dictionary_theme_B);
        return new PairCollection(dictionary_filename,
                dictionary_subdirectory,
                dictionary_sourceUrl,
                dictionary_theme_A,
                dictionary_theme_B, dt.getPairs());
    }

    static String findMetadata(String key, List<EntryType> metadata) {
        if (key == null) {
            return null;
        }
        for (EntryType entryType : metadata) {
            if (entryType == null) {
                continue;
            }
            if (key.equals(entryType.getKey())) {
                return entryType.getValue();
            }
        }
        return null;
    }
}
