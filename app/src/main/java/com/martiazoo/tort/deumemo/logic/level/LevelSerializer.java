package com.martiazoo.tort.deumemo.logic.level;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import com.martiazoo.tort.deumemo.logic.card.CardState;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.done.LevelCompleteInfo;
import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.util.*;

public class LevelSerializer {
    private static final String TAG = "LevelSerializer";
    private static final String KEY = "LevelSerializer-Version1";
    public final static LevelSerializer instance = new LevelSerializer();
    private final XStream xStream = configureXStream();

    private XStream configureXStream() {
        XStream xstream = new XStream() {

            String[] includes = new String[]{"levelCompleteInfo", "dimension", "pairs"};

            @Override
            protected MapperWrapper wrapMapper(MapperWrapper next) {
                return new MapperWrapper(next) {
                    @Override
                    public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                        if (definedIn == Level.class) {
                            for (String include : includes) {
                                if (include.equalsIgnoreCase(fieldName)) {
                                    return true;
                                }
                            }
                            return false;
                        }
                        return super.shouldSerializeMember(definedIn, fieldName);
                    }
                };
            }
        };
//        xstream.alias("level", Level.class);
        xstream.alias("pair", PairType.class);
        xstream.alias("version1", HolderVersion1.class);
        xstream.alias("entry", AbstractMap.SimpleEntry.class);
        xstream.alias("GFElement", GFElement.class);
        return xstream;
    }

    @Nullable
    public String toString(Level level) {
        try {
            String s = internalToString(level);
            return s;
        } catch (Throwable t) {
            Log.e(TAG, t.getMessage(), t);
        }
        return null;
    }

    @Nullable
    public String internalToString(Level level) {
        String s;
        Object o = HolderVersion1.wrap(level);
        s = xStream.toXML(o);
        return s;
    }

    @Nullable
    public HolderVersion1 fromString(String s) {
        try {
            return internalFromString(s);
        } catch (Throwable t) {
            Log.e(TAG, t.getMessage(), t);
        }
        return null;
    }

    @Nullable
    HolderVersion1 internalFromString(String s) {
        Level l;
        HolderVersion1 o = (HolderVersion1) xStream.fromXML(s);
        return o;
    }

    public void save(Level l, Bundle savedInstanceState) {
        String s = toString(l);
        if (s == null) {
            Log.e(TAG, "cannot save level as serialized to null");
            return;
        }
        savedInstanceState.putString(KEY, s);
    }

    public StateLevelBuilder load(Context ctx, Bundle savedInstanceState) {
        String s = savedInstanceState.getString(KEY);
        if (s == null) {
            Log.d(TAG, "restored empty string by key: " + KEY);
            return null;
        }
        HolderVersion1 holder = fromString(s);
        if (holder == null) {
            Log.d(TAG, "cannot restore level from string " + s);
            return null;
        }
        return new StateLevelBuilder(ctx, holder);
    }

    public static class HolderVersion1 {
        private LevelCompleteInfo levelCompleteInfo = new LevelCompleteInfo();
        private Point dimension;
        private List<GFElement> elements;
        private long savedAt;

        static HolderVersion1 wrap(Level l) {
            HolderVersion1 holder = new HolderVersion1();
            holder.savedAt = System.currentTimeMillis();
            holder.levelCompleteInfo = l.getLevelCompleteInfo();
            holder.dimension = l.getPuzzleDimension();
//            holder.pairs = l.getPairs();
            List<GFElement> list = new ArrayList<>(l.getElements());
            Collections.sort(list, new Comparator<GFElement>() {
                @Override
                public int compare(GFElement lhs, GFElement rhs) {
                    return lhs.getPosition() - rhs.getPosition();
                }
            });

            holder.elements = new ArrayList<>();
            for (GFElement element : list) {
                GFElement e = element.copy();
                holder.elements.add(e);
            }

            normalize(holder);
            return holder;
        }

        private static void normalize(HolderVersion1 holder) {
            /*only back and hidden states are allowed*/
            for (GFElement e : holder.elements) {
                if (e.getCardState() == CardState.BACK) {
                    continue;
                }
                if (e.getCardState() == CardState.HIDDEN) {
                    continue;
                }
                e.setCardState(CardState.BACK);
            }
            /*hidden card only in pairs. Forcing hide for opened card*/
            for (GFElement e : holder.elements) {
                if (e.getCardState() != CardState.HIDDEN) {
                    continue;
                }
                GFElement e2 = GFElement.findPair(e, holder.elements);
                e2.setCardState(CardState.HIDDEN);
            }
            /*validate even number for pairs*/
            int hiddens = 0;
            for (GFElement e : holder.elements) {
                if (e.getCardState() != CardState.HIDDEN) {
                    continue;
                }
                hiddens++;
            }
            if (hiddens % 2 != 0) {
                throw new IllegalStateException("hiddens % 2 != 0");
            }
            /*number of correct attempts cannot be greater then number of hidden cards*/
            holder.levelCompleteInfo.correctAttempts = hiddens / 2;
        }
    }

    public static class StateLevelBuilder extends AbstractLevelBuilder {
        private static final String TAG = "StateLevelBuilder";
        HolderVersion1 holder;
        Context ctx;

        public StateLevelBuilder(Context ctx, HolderVersion1 holder) {
            this.ctx = ctx;
            this.holder = holder;
        }

        @Override
        public Level nextLevel() {
            Level l = new Level(ctx);
            long duration = holder.savedAt - holder.levelCompleteInfo.startedMs;
            long currentMs = System.currentTimeMillis();
            long startedMs = currentMs - duration;
            holder.levelCompleteInfo.startedMs = startedMs;
            l.setLevelCompleteInfo(holder.levelCompleteInfo);
            l.init(holder.dimension, toPairs(holder.elements), holder.elements);
            return l;
        }

        List<PairType> toPairs(List<GFElement> elements) {
            Set<PairType> pairSet = new HashSet<>();
            for (GFElement e : elements) {
                pairSet.add(e.getPair());
            }
            if (pairSet.contains(null)) {
                throw new IllegalStateException("pairs.contains(null)");
            }
            List<PairType> pairs = new ArrayList<>(pairSet);
            return pairs;
        }

    }
}
