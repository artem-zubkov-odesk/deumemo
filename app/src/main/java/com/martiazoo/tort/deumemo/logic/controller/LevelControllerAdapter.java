package com.martiazoo.tort.deumemo.logic.controller;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.ui.AnimationListenerAdapter;
import com.martiazoo.tort.deumemo.ui.card.CardView;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;
import com.martiazoo.tort.deumemo.ui.cardtable.CardTableActivity;
import com.martiazoo.tort.deumemo.util.threading.NullableRunnable;
import com.martiazoo.tort.deumemo.util.threading.StepLoadTracker;
import com.martiazoo.tort.deumemo.util.threading.forceable.ForceablePool;

import java.util.List;

public class LevelControllerAdapter implements LevelController {
    private static final String TAG = "LevelControllerAdapter";
    protected Context ctx;
    protected Level l;
    protected List<GFElement> all;
    protected ViewResolver vr;
    protected Handler handler;
    protected final ForceablePool pool = new ForceablePool();
    protected final Object poolKey = this;

    @Override
    public void initialize(Context ctx, Level l, List<GFElement> all, ViewResolver vr) {
        this.ctx = ctx;
        this.all = all;
        this.vr = vr;
        this.l = l;

        handler = ((CardTableActivity)ctx).getHandler();
        pool.setHandler(handler);
    }

    @Override
    public void animatePopulation(List<CardView> all, final Runnable callback) {
        StepLoadTracker stepLoadTracker = new StepLoadTracker(
                all.size()
                , NullableRunnable.getFirst(callback)
                , 5000
                , handler);
        stepLoadTracker.waitLoading();
        int c = 0;
        for (View v : all) {
            animateCardAppear((CardView) v, /*animation.getDuration()*/300 / 10 * c, stepLoadTracker);
            c++;
        }
    }

    @Override
    public void heartbeat() {
    }

    @Override
    public void elementClicked(GFElement e) {
    }

    @Override
    public void tableClicked() {
    }

    @Override
    public void destroy(Runnable... callback) {
        NullableRunnable.getFirst(callback).run();
    }

    public static void animateCardAppear(CardView v, long startOffset, final Runnable... callback) {
        Animation animation = AnimationUtils.loadAnimation(v.getContext(), R.anim.level_populate_from_dot_distention);
        animation.setStartOffset(startOffset);
        animation.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                NullableRunnable.getFirst(callback).run();
            }
        });
        v.startAnimation(animation);
        v.invalidate();
    }
}
