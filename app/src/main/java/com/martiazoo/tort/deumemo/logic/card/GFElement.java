package com.martiazoo.tort.deumemo.logic.card;

import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;

import java.util.List;

public class GFElement {
    /*immutable*/
    private final PairType pair;
    private final boolean isA;

    /*mutable*/
    private CardState cardState = CardState.BACK;
    private int position = -1;

    public GFElement(PairType pair, boolean isA) {
        this.pair = pair;
        this.isA = isA;
    }

    public PairType getPair() {
        return pair;
    }

    public CardState getCardState() {
        return cardState;
    }

    public void setCardState(CardState cardState) {
        this.cardState = cardState;
    }

    public String getString() {
        return getString(isA, pair);
    }

    private String getString(boolean a, PairType p) {
        if (a) {
            return p.getA();
        } else {
            return p.getB();
        }
    }

    public boolean isPair(String s) {
        if (s == null) {
            throw new IllegalArgumentException();
        }
        return s.hashCode() == getString(!isA, pair).hashCode();
    }

    public boolean isPair(PairType p) {
        return isPair(getString(!isA, p));
    }

    public boolean isPair(GFElement e) {
        return isPair(e.getPair());
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public boolean isA() {
        return isA;
    }

    public GFElement copy() {
        GFElement e = new GFElement(this.getPair(), this.isA());
        e.cardState = this.cardState;
        e.position = this.position;
        return e;
    }

    @Override
    public String toString() {
        return "GFElement{" +
                "cardState=" + cardState +
                ", isA=" + isA +
                ", pair=" + pair +
                ", ihc=" + System.identityHashCode(this) +
                '}';
    }

    public static GFElement findPair(GFElement src, List<GFElement> elements) {
        for (GFElement element : elements) {
            if (isPair(src, element)) {
                return element;
            }
        }
        return null;
    }

    public static boolean isPair(GFElement src, GFElement element) {
        return element != src && element.getPair() == src.getPair();
    }

}
