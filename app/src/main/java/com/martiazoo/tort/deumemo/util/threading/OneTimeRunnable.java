package com.martiazoo.tort.deumemo.util.threading;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class OneTimeRunnable implements Runnable {
    private final AtomicBoolean called = new AtomicBoolean();


    @Override
    public final void run() {
        if (called.compareAndSet(false, true)) {
            oneTimeRun();
        }
    }

    public abstract void oneTimeRun();

    static public Runnable wrap(Runnable r) {
        final Runnable r2 = NullableRunnable.wrap(r);
        return new OneTimeRunnable() {
            @Override
            public void oneTimeRun() {
                r2.run();
            }
        };
    }
}