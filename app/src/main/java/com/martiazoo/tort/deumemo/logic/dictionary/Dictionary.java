package com.martiazoo.tort.deumemo.logic.dictionary;

import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Transformer;
import org.apache.commons.collections4.iterators.IteratorChain;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class Dictionary implements Iterable<PairType> {
    private static final String TAG = "Dictionary";
    private List<PairCollection> list;

    public Dictionary(List<PairCollection> list) {
        this.list = list;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Iterator<PairType> iterator() {
        Collection<Iterator<PairType>> is = CollectionUtils.collect(list, new Transformer() {
            public Object transform(Object o) {
                return ((PairCollection) o).pairs.iterator();
            }
        });
        return new IteratorChain(is);
    }

    public boolean isEmpty() {
        for (PairCollection pairCollection : list) {
            if (!pairCollection.pairs.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
