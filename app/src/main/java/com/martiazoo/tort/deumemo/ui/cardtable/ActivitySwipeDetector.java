package com.martiazoo.tort.deumemo.ui.cardtable;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class ActivitySwipeDetector implements View.OnTouchListener {
    private static final String TAG = "ActivitySwipeDetector";
    private static long MAX_TOUCH_TIME_MS = 1500;
    private static final int MIN_DISTANCE = 80;

    private float downX, downY, upX, upY;
    private boolean isTouchReleased = true;
    private long touchTTL;

    public boolean onRightToLeftSwipe() {
        Log.i(TAG, "RightToLeftSwipe!" + getClass().getSimpleName());
        return false;
    }

    public boolean onLeftToRightSwipe() {
        Log.i(TAG, "LeftToRightSwipe! " + getClass().getSimpleName());
        return false;
    }

    public boolean onTopToBottomSwipe() {
        Log.i(TAG, "onTopToBottomSwipe!" + getClass().getSimpleName());
        return false;
    }

    public boolean onBottomToTopSwipe() {
        Log.i(TAG, "onBottomToTopSwipe!" + getClass().getSimpleName());
        return false;
    }

    public boolean onSimpleTap() {
        Log.i(TAG, "onTap!" + getClass().getSimpleName());
        return false;
    }

    public boolean onTouch(View v, MotionEvent event) {
        if (!isTouchReleased && System.currentTimeMillis() > touchTTL) {
            isTouchReleased = true;
        }
        if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE) {
            if (isTouchReleased) {
                downX = event.getX();
                downY = event.getY();
                isTouchReleased = false;
                touchTTL = System.currentTimeMillis() + MAX_TOUCH_TIME_MS;
                Log.d(TAG, "onTouch coordinates recorded for: " + getClass().getSimpleName());
            }
            return false;
        }
        if (event.getAction() == MotionEvent.ACTION_UP) {
            if (isTouchReleased) {
                /*nothing to do as no startpoint recorded*/
                return false;
            }
            isTouchReleased = true;
            upX = event.getX();
            upY = event.getY();

            float deltaX = downX - upX;
            float deltaY = downY - upY;

            // swipe horizontal?
            if (Math.abs(deltaX) > Math.abs(deltaY)) {
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // left or right
                    if (deltaX < 0) {
                        return this.onLeftToRightSwipe();
                    }
                    if (deltaX > 0) {
                        return this.onRightToLeftSwipe();
                    }
                }
            } else {
                // swipe vertical?
                if (Math.abs(deltaY) > MIN_DISTANCE) {
                    // top or down
                    if (deltaY < 0) {
                        return this.onTopToBottomSwipe();
                    }
                    if (deltaY > 0) {
                        return this.onBottomToTopSwipe();
                    }
                }
            }
            /*no swipe but tap has happened*/
            return this.onSimpleTap();
        }
        return false;
    }
}