package com.martiazoo.tort.deumemo.ui.wordlist;


public interface VisibilityListener {
    void notifyVisibilityChanged(boolean isShown);

    class Adapter implements VisibilityListener {
        @Override
        public void notifyVisibilityChanged(boolean isShown) {
            /*do nothing here*/
        }
    }

    VisibilityListener DUMMY = new Adapter();
}
