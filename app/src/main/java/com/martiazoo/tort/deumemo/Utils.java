package com.martiazoo.tort.deumemo;


import android.animation.Animator;
import android.animation.AnimatorSet;
import android.content.res.TypedArray;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public final class Utils {
    private static final String TAG = "Utils";

    private Utils() {
    }

    public static List<View> childrenAsList(ViewGroup vg) {
        List<View> linear = new ArrayList<>();
        int count = vg.getChildCount();
        for (int i = 0; i < count; i++) {
            final View v = vg.getChildAt(i);
            linear.add(v);
        }
        return linear;
    }

    public static int getActionBarHeight(ActionBarActivity activity) {
        final TypedArray styledAttributes = activity.getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize});
        int mActionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();
        return mActionBarSize;
    }

    public static long durationWithStartDelay(Animator a) {
        if (a instanceof AnimatorSet) {
            long max = 0;
            for (Animator animator : ((AnimatorSet) a).getChildAnimations()) {
                max = Math.max(durationWithStartDelay(animator), max);
            }
            return max;
        }
        return a.getStartDelay() + a.getDuration();
    }


    public static String[] splitWords(String s) {
        String[] lines = s.split(" ");
        return lines;
    }

    public static String removeSpaces(String word) {
        word.trim();
        for (int i = 0; i < word.length() - 1; i++) {
            if (word.charAt(i) == '-' ) {
                if (word.charAt(i+1) == ' ') {
                    if (i > 0) {
                        word = word.substring(0, i) + word.substring(i + 2);
                    }
                }
            }
        }
        return word;
    }

}
