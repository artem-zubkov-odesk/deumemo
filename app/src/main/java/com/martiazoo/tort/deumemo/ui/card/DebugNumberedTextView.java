package com.martiazoo.tort.deumemo.ui.card;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

/*todo debug purposes*/
public class DebugNumberedTextView extends CardView {
    private static final String TAG = "NumberedTextView";
    private static final AtomicInteger COUNTER = new AtomicInteger();
    static final Paint tp;
    int n = COUNTER.getAndIncrement();

    static {
        tp = new Paint(Paint.ANTI_ALIAS_FLAG);
        tp.setColor(Color.BLACK);
        tp.setStrokeWidth(1);
        tp.setStyle(Paint.Style.STROKE);
        tp.setStyle(Paint.Style.FILL_AND_STROKE);
        tp.setTextSize(15);
    }

    {
        Log.d(TAG, "constructed n: " + n + "  ihc:" + System.identityHashCode(this));
    }

    public DebugNumberedTextView(Context context, CardTextRenderer cardTextRenderer, CardBitmapRenderer renderer) {
        super(context, cardTextRenderer, renderer);
    }

    public DebugNumberedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        canvas.drawText(String.valueOf(n), 10, 10, tp);
        canvas.drawText(String.valueOf(getPosition()), 40, 10, tp);
        canvas.drawText(String.valueOf(System.identityHashCode(this)), 10, getMeasuredHeight() - 65, tp);
        canvas.drawText(String.valueOf(System.identityHashCode(this.getElement())), 10, getMeasuredHeight() - 45, tp);
        canvas.drawText(String.valueOf(this.getElement().getCardState()), 10, getMeasuredHeight() - 25, tp);
        canvas.drawText(String.valueOf(System.currentTimeMillis()), 10, getMeasuredHeight() - 5, tp);
    }
}
