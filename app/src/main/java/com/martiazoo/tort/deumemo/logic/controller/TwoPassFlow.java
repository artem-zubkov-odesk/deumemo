package com.martiazoo.tort.deumemo.logic.controller;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.Utils;
import com.martiazoo.tort.deumemo.logic.autoplay.LevelAutoPlayer;
import com.martiazoo.tort.deumemo.logic.card.CardState;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.ui.AnimationListenerAdapter;
import com.martiazoo.tort.deumemo.ui.card.CardView;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;
import com.martiazoo.tort.deumemo.ui.cardtable.CardTableActivity;
import com.martiazoo.tort.deumemo.util.threading.Heartbeat;
import com.martiazoo.tort.deumemo.util.threading.NullableRunnable;
import com.martiazoo.tort.deumemo.util.threading.OneTimeRunnable;
import com.martiazoo.tort.deumemo.util.threading.StepLoadTracker;
import com.martiazoo.tort.deumemo.util.threading.forceable.ForceablePool;
import org.apache.commons.collections4.Predicate;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class TwoPassFlow extends LevelControllerAdapter implements Heartbeat.Listener {
    private static final String TAG = "TwoPassFlow";
    private int clickNumber = -1;
    private GFElement even;
    private GFElement odd;
    private int completedCards;
    private final AtomicBoolean levelCompletedCalled = new AtomicBoolean();

    protected final LevelAutoPlayer autoPlayer = LevelAutoPlayer.instance;
    protected boolean autoplayWasRequestedOnce;


    @Override
    public void initialize(Context ctx, Level l, List<GFElement> all, ViewResolver vr) {
        super.initialize(ctx, l, all, vr);
        Heartbeat.get().register(this);
        for (GFElement element : all) {
            if (element.getCardState() == CardState.HIDDEN) {
                completedCards++;
            }
        }
        boolean isAutoplay = autoPlayer.isStarted();
        autoPlayer.stop()
                .clear()
                .setContext(ctx)
                .setListener(((CardTableActivity) ctx).getAutoPlayerListener())
                .setLevel(l)
                .setPool(handler)
                .setShuffleLevelRunnable(((CardTableActivity) ctx).shuffleLevelRunnable)
                .setIsLevelDoneDialogOnScreenPredicate(((CardTableActivity) ctx).isLevelDoneDialogOnScreenPredicate)
                .setCardOpenPredicate(new Predicate<GFElement>() {
                    @Override
                    public boolean evaluate(GFElement object) {
                        triggerElementState(object);
                        return false;
                    }
                });
        if (isAutoplay) {
            autoPlayer.start();
        }
    }

    @Override
    public void heartbeat() {
        super.heartbeat();
        checkCompleted();
        autoComplete();
    }

    private void autoComplete() {
        if (autoplayWasRequestedOnce) {
            return;
        }
        if (autoPlayer.isStarted()) {
            return;
        }
        boolean lastPair = (all != null && all.size() == completedCards + 2);
        if (!lastPair) {
            return;
        }
        Log.d(TAG, "last pair detected, registering autoplayWasRequestedOnce");
        autoplayWasRequestedOnce = true;

        pool.getSequence(poolKey).register(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "calling autoPlayer.start after timeout");
                if (autoPlayer.isStarted()) {
                    return;
                }
                autoPlayer.start();
            }
        }, 3000, "autoComplete autoPlayer.start()");
    }

    private void checkCompleted() {
        if (levelCompletedCalled.get()) {
            return;
        }
        boolean completed = (all != null && all.size() == completedCards);
        if (!completed) {
            return;
        }
        Log.d(TAG, "level completed detected");
        if (levelCompletedCalled.compareAndSet(false, true)) {
            /*otherwise smart AI click rendomize by itself*/
            if (autoplayWasRequestedOnce) {
                autoPlayer.stop();
            /*keep enabled otherwise*/
            }

            Log.d(TAG, "level completed dialog executed");
            pool.getSequence(poolKey).register(new Runnable() {
                @Override
                public void run() {
                    pool.forceForeverAll();
                    ((CardTableActivity) (Object) ctx).showLevelCompleteDialog();
                    Heartbeat.get().unregister(TwoPassFlow.this);
                }
            }, 3000, "level completed dialog timeout");
        }
    }

    @Override
    public void tableClicked() {
        super.tableClicked();
        autoPlayer.stop();
        pool.forceAll();
    }


    private void triggerElementState(final GFElement e) {
//        Log.d(TAG, "B thread: " + Thread.currentThread().getName());
//        Log.d(TAG, "B thread hc: " + System.identityHashCode(Thread.currentThread()));
//        Log.d(TAG, "B completedCards: " + completedCards);
//        Log.d(TAG, "B this: " + this);
        Log.d(TAG, "even " + even);
        Log.d(TAG, "odd  " + odd);

        /*complete existing animations*/
        boolean animationForced = pool.forceAll();
        if (animationForced) {
            return;
        }

        if ((even == e || odd == e) && (even == null || odd == null)) {
            Log.d(TAG, "click on the same element");
            /*closing card if it was open*/
            clickNumber = -1;
            even = null;
            odd = null;
            pool.getSequence(e).force();
            faceDownCards(Arrays.asList(e), 0);
            return;
        }
        /*if card is gone - do nothing*/
        if (e.getCardState() == CardState.HIDDEN) {
            return;
        }

        clickNumber++;
        clickNumber %= 2;
        if (clickNumber == 0) {
            even = e;
            Log.d(TAG, "even set to " + even);
        } else {
            odd = e;
            Log.d(TAG, "odd  set to " + odd);
        }
        if (even != e && odd != e) {
            throw new IllegalStateException();
        }

        if (even == null || odd == null) {
            Log.d(TAG, "only one element clicked - opening");
            if (e.getCardState() == CardState.BACK) {
                CardView v = vr.toView(e);
                animateCardOpened(v, pool);
            }
            return;
        }
        final boolean isPair = even.isPair(odd);
        final List<GFElement> opened = Arrays.asList(even, odd);
        final GFElement other = (e == odd) ? even : odd;

        if (e.getCardState() == CardState.BACK) {
            final CardView v = vr.toView(e);
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    if (isPair) {
                        completedCards += 2;
                        l.getLevelCompleteInfo().correctAttempts++;
                        CardView vOther = vr.toView(other);
                        animateCardCorrect(v);
                        animateCardCorrect(vOther);
                        pairCompleteCards(v, vOther);
                    } else {
                        l.getLevelCompleteInfo().incorrectAttempts++;
                        CardView vOther = vr.toView(other);
                        animateCardIncorrect(v);
                        animateCardIncorrect(vOther);
                        faceDownCards(opened);
                    }
                }
            };
            animateCardOpened(v, pool, r);
        }

        clickNumber = -1;
        even = null;
        odd = null;
        heartbeat();
    }

    @Override
    public void elementClicked(final GFElement e) {
        super.elementClicked(e);
        if (autoPlayer.isStarted()) {
            autoPlayer.stop();
            /*commented to allow player open that card!*/
//            return;
        }
        triggerElementState(e);
    }

    private void pairCompleteCards(final CardView v, final CardView vOther, final Runnable... callback) {
        final Runnable r = NullableRunnable.getFirst(callback);

        pool.getSequence(v.getElement()).register(new Runnable() {
            @Override
            public void run() {
                animateCardDisappear(v, r);
            }
        }, 3500, "pairCompleteCards for " + v.getElement());
        pool.getSequence(vOther.getElement()).register(new Runnable() {
            @Override
            public void run() {
                animateCardDisappear(vOther, r);
            }
        }, 3500, "pairCompleteCards for " + vOther.getElement());
    }

    private void faceDownCards(List<GFElement> opened) {
        for (final GFElement e : opened) {
            faceDownCard(e, 2500);
        }
    }

    private void faceDownCards(List<GFElement> opened, long timeoutMs) {
        for (final GFElement e : opened) {
            faceDownCard(e, timeoutMs);
        }
    }

    public void faceDownCard(final GFElement e, long timeoutMs, final Runnable... callback) {
        Log.d(TAG, "faceDownCards called; timeoutMs:" + timeoutMs + " ;currentThread: " + Thread.currentThread());
        pool.getSequence(e).register(new Runnable() {
            @Override
            public void run() {
                CardView v = vr.toView(e);
                animateCardHide(v, pool, callback);
            }
        }, timeoutMs, "faceDownCard for " + e.getPair());
    }

    @Override
    public void animatePopulation(final List<CardView> all, final Runnable callback) {
        final StepLoadTracker stepLoadTracker = new StepLoadTracker(
                all.size()
                , NullableRunnable.getFirst(callback)
                , 5000
                , handler);
        stepLoadTracker.waitLoading();
        for (CardView cardView : all) {
            cardView.getElement().setCardState(CardState.HIDDEN);
        }
        final Runnable faceDownAll = new Runnable() {
            @Override
            public void run() {
                int c = 0;
                for (View v : all) {
                    CardView cardView = (CardView) v;
                    faceDownCard(cardView.getElement(), 300 / 10 * c, stepLoadTracker);
                    c++;
                }
            }
        };
        final Runnable smallWait = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(faceDownAll, 1000);
            }
        };
        animatePopulationInternal(all, smallWait);
    }

    public void animatePopulationInternal(List<CardView> all, final Runnable callback) {
        final StepLoadTracker stepLoadTracker = new StepLoadTracker(
                all.size()
                , NullableRunnable.getFirst(callback)
                , 5000
                , handler);
        stepLoadTracker.waitLoading();
        int c = 0;
        for (View v : all) {
            final CardView cardView = (CardView) v;
            final GFElement e = cardView.getElement();
            final long timeoutMs = 300 / 10 * c;
            pool.getSequence(e).register(new Runnable() {
                @Override
                public void run() {
                    final CardView v = cardView;
                    cardView.getElement().setCardState(CardState.TOP);
                    animateCardAppear((CardView) v, /*animation.getDuration()*/0, stepLoadTracker);
                }
            }, timeoutMs, "animatePopulationInternal for " + e.getPair());
            c++;
        }
    }

    @Override
    public void destroy(final Runnable... callback) {
        Heartbeat.get().unregister(this);
//        if (autoplayWasRequestedOnce) {
//            autoPlayer.stop();
//            /*keep enabled otherwise*/
//        }
        for (final GFElement e : all) {
            final CardView v = vr.toView(e);
            e.setCardState(CardState.HIDDEN);
            v.invalidate();
//            v.setAlpha(0);
//            v.setVisibility(View.INVISIBLE);
        }
        pool.forceForeverAll(NullableRunnable.getFirst(callback));
    }

    static void animateCardCorrect(final CardView v, final Runnable... callback) {
        Animation animation = AnimationUtils.loadAnimation(v.getContext(), R.anim.zoom_in);
        animation.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                NullableRunnable.getFirst(callback).run();
            }
        });
        v.startAnimation(animation);
    }

    static void animateCardIncorrect(final CardView v, final Runnable... callback) {
        Animation animation = AnimationUtils.loadAnimation(v.getContext(), R.anim.tilt_horizontal);
        animation.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                NullableRunnable.getFirst(callback).run();
            }
        });
        v.startAnimation(animation);
    }

    static void animateCardDisappear(final CardView v, final Runnable... callback) {
        Animation animation = AnimationUtils.loadAnimation(v.getContext(), R.anim.pair_matches_card_to_dot_distention);
        animation.setAnimationListener(new AnimationListenerAdapter() {
            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(TAG, "v visibility is GONE");
                v.getElement().setCardState(CardState.HIDDEN);
                v.setVisibility(View.GONE);
                v.invalidate();
                NullableRunnable.getFirst(callback).run();
            }
        });
        v.startAnimation(animation);
    }

    static void animateCardHide(final CardView v, ForceablePool pool, final Runnable... callback) {
        final Runnable toState = OneTimeRunnable.wrap(new Runnable() {
            @Override
            public void run() {
                v.getElement().setCardState(CardState.BACK);
                v.invalidate();
            }
        });

        Animator flipBegin = AnimatorInflater.loadAnimator(v.getContext(), R.animator.card_flip_left_out);
        flipBegin.setTarget(v);
        flipBegin.start();

        final Animator flipEnd = AnimatorInflater.loadAnimator(v.getContext(), R.animator.card_flip_left_out2);
        flipEnd.setTarget(v);
        flipEnd.start();

        long duration;
        duration = Utils.durationWithStartDelay(flipBegin);
        pool.getSequence(v.getElement()).register(toState, duration, "animateCardHide#toState for " + v.getElement().getPair());
        duration = Utils.durationWithStartDelay(flipEnd);
        pool.getSequence(v.getElement()).register(NullableRunnable.getFirst(callback), duration, "animateCardHide#flipEnd for " + v.getElement().getPair());
    }

    static void animateCardOpened(final CardView v, ForceablePool pool, final Runnable... callback) {
        final Runnable toState = OneTimeRunnable.wrap(new Runnable() {
            @Override
            public void run() {
                v.getElement().setCardState(CardState.TOP);
                v.invalidate();
            }
        });

        Animator flipBegin = AnimatorInflater.loadAnimator(v.getContext(), R.animator.card_flip_left_out);
        flipBegin.setTarget(v);
        flipBegin.start();

        final Animator flipEnd = AnimatorInflater.loadAnimator(v.getContext(), R.animator.card_flip_left_out2);
        flipEnd.setTarget(v);
        flipEnd.start();

        long duration;
        duration = Utils.durationWithStartDelay(flipBegin);
        pool.getSequence(v.getElement()).register(toState, duration, "animateCardHide#toState for " + v.getElement().getPair());
        duration = Utils.durationWithStartDelay(flipEnd);
        pool.getSequence(v.getElement()).register(NullableRunnable.getFirst(callback), duration, "animateCardHide#flipEnd for " + v.getElement().getPair());

    }
}
