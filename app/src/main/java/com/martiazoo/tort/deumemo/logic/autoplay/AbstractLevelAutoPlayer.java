package com.martiazoo.tort.deumemo.logic.autoplay;

import android.content.Context;
import android.os.Handler;
import android.os.PowerManager;
import android.util.Log;
import com.google.common.base.Preconditions;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.util.threading.Heartbeat;
import org.apache.commons.collections4.Predicate;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public abstract class AbstractLevelAutoPlayer implements Heartbeat.Listener {
    private static final String TAG = "ALevelAutoPlayer";
    protected final Object poolKey = this;
    protected final AtomicBoolean isStopped = new AtomicBoolean(true);
    private final AtomicBoolean isStarted = new AtomicBoolean(false);
    private final AtomicReference<Thread> singleThreadAllowed = new AtomicReference<>();
    private Context ctx;
    protected Level level;
    protected Handler pool;
    protected Predicate<GFElement> cardOpenPredicate;
    protected Predicate<Void> isLevelDoneDialogOnScreenPredicate = new Predicate<Void>() {
        @Override
        public boolean evaluate(Void object) {
            /*false by default*/
            return false;
        }
    };
    protected Runnable shuffleLevelRunnable;
    private Listener listener = Listener.Abstract.DUMMY;
    private PowerManager powerManager;

    public boolean isStarted() {
        return isStarted.get();
    }

    protected void register(final Runnable r, long delayMs, final String s) {
        pool.postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "register calling " + s + " runnable: " + r);
                r.run();
            }
        }, delayMs);
//        pool.getSequence(poolKey).register(delayMiddle, 1000, "LevelAutoPlayer delayMiddle");
    }

    public AbstractLevelAutoPlayer stop() {
        Log.d(TAG, "calling stop...");
        throwWrongThread();
        if (isStopped.get()) {
            return this;
        }
        isStopped.set(true);
        isStarted.set(false);
        listener.stopped();
        return this;
    }

    public void start() {
        throwWrongThread();
        validate();
        isStopped.set(false);
        isStarted.set(true);
        listener.started();
    }

    protected void throwWrongThread() {
        if (singleThreadAllowed.compareAndSet(null, Thread.currentThread())) {
            return;
        }
        Preconditions.checkArgument(singleThreadAllowed.get() == Thread.currentThread(), "use instance only in single thread");
    }

    private void throwNotStopped() {
        Preconditions.checkArgument(isStopped.get(), "not stopped");
    }

    public AbstractLevelAutoPlayer setContext(Context ctx) {
        throwWrongThread();
        throwNotStopped();
        this.ctx = ctx;
        powerManager = (PowerManager) ctx.getSystemService(ctx.POWER_SERVICE);
        return this;
    }

    public AbstractLevelAutoPlayer setShuffleLevelRunnable(Runnable shuffleLevelRunnable) {
        throwWrongThread();
        throwNotStopped();
        this.shuffleLevelRunnable = shuffleLevelRunnable;
        return this;
    }

    public AbstractLevelAutoPlayer setIsLevelDoneDialogOnScreenPredicate(Predicate<Void> isLevelDoneDialogOnScreenPredicate) {
        throwWrongThread();
        throwNotStopped();
        this.isLevelDoneDialogOnScreenPredicate = isLevelDoneDialogOnScreenPredicate;
        return this;
    }

    public AbstractLevelAutoPlayer setLevel(Level level) {
        throwWrongThread();
        throwNotStopped();
        this.level = level;
        return this;
    }

    public AbstractLevelAutoPlayer setListener(Listener listener) {
        throwWrongThread();
        throwNotStopped();
        this.listener = listener;
        if (this.listener == null) {
            this.listener = Listener.Abstract.DUMMY;
        }
        return this;
    }

    public AbstractLevelAutoPlayer setCardOpenPredicate(Predicate<GFElement> cardOpenPredicate) {
        throwWrongThread();
        throwNotStopped();
        this.cardOpenPredicate = cardOpenPredicate;
        return this;
    }

    public AbstractLevelAutoPlayer setPool(Handler pool) {
        throwWrongThread();
        throwNotStopped();
        this.pool = pool;
        return this;
    }

    public AbstractLevelAutoPlayer clear() {
        throwWrongThread();
        throwNotStopped();
        this.ctx = null;
        this.level = null;
        this.cardOpenPredicate = null;
        this.pool = null;
        listener = null;
        shuffleLevelRunnable = null;
        isLevelDoneDialogOnScreenPredicate = null;
        isStarted.set(false);
        isStopped.set(true);
        return this;
    }

    @Override
    public void heartbeat() {
        if (isStopped.get()) {
            return;
        }
        if (!isScreenOn()) {
            Log.d(TAG, "stopping autoplay as screen is off");
            stop();
        }
    }

    protected boolean isScreenOn(){
        return  powerManager.isScreenOn();
    }

    private void validate() {
        throwWrongThread();
        throwNotStopped();
        Preconditions.checkNotNull(ctx);
        Preconditions.checkNotNull(level);
        Preconditions.checkNotNull(cardOpenPredicate);
        Preconditions.checkNotNull(pool);
        Preconditions.checkNotNull(shuffleLevelRunnable);
        Preconditions.checkNotNull(isLevelDoneDialogOnScreenPredicate);
    }

    public interface Listener {
        void started();

        void stopped();

        static class Abstract implements Listener {
            public static Abstract DUMMY = new Abstract();

            @Override
            public void started() {

            }

            @Override
            public void stopped() {

            }
        }
    }
}
