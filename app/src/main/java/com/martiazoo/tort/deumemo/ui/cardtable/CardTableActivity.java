package com.martiazoo.tort.deumemo.ui.cardtable;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.google.common.base.Supplier;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.Utils;
import com.martiazoo.tort.deumemo.logic.autoplay.AbstractLevelAutoPlayer;
import com.martiazoo.tort.deumemo.logic.dictionary.Dictionaries;
import com.martiazoo.tort.deumemo.logic.level.DictionaryLevelBuilder;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.logic.scope.ConfigProvider;
import com.martiazoo.tort.deumemo.ui.AutoplayHelper;
import com.martiazoo.tort.deumemo.ui.actionbar.MenuHelper;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;
import com.martiazoo.tort.deumemo.ui.level.done.LevelDoneStaticHelper;
import com.martiazoo.tort.deumemo.ui.level.start.StartLevelAnimator;
import com.martiazoo.tort.deumemo.ui.wordlist.VisibilityListener;
import com.martiazoo.tort.deumemo.ui.wordlist.WordListHelper;
import org.apache.commons.collections4.Predicate;

import java.util.Collections;
import java.util.List;

public class CardTableActivity extends ActionBarActivity {
    private static final String TAG = "CardTableActivity";

    private volatile Level level;
    private Dictionaries ds;
    private MenuHelper menuHelper;
    private WordListHelper wordListHelper;
    private AutoplayHelper autoplayHelper;
    private LevelDoneStaticHelper levelDoneHelper;
    private Handler handler;
    public ConfigProvider configProvider;
    public final Point displaySize = new Point();
    public final Runnable shuffleLevelRunnable = new Runnable() {
        @Override
        public void run() {
            shuffleLevel();
        }
    };

    private final Supplier<Level> levelSupplier = new Supplier<Level>() {
        @Override
        public Level get() {
            return level;
        }
    };
    private SaveStateController saveStateController = new SaveStateController(this, shuffleLevelRunnable, levelSupplier);

    public final Predicate<Void> isLevelDoneDialogOnScreenPredicate = new Predicate<Void>() {
        @Override
        public boolean evaluate(Void object) {
            return levelDoneHelper.isShown();
        }
    };

    public Handler getHandler() {
        return handler;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void decorateWindow() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    0x0
//                | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        } else {
            // do something for phones running an SDK before lollipop
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
        }
    }

    private void decorateActionBar() {
        ViewGroup wrapperView = (ViewGroup) findViewById(R.id.above_root_layoutAA);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_custom_layout);
        TextView textView = (TextView) findViewById(R.id.actionbar_application_title);
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) textView.getLayoutParams();
        params.setMargins(Utils.getActionBarHeight(this), 0, 0, 0);

        final ImageView iv = (ImageView) this.findViewById(R.id.menuSwipeHintTop);
        ((ViewGroup) iv.getParent()).removeView(iv);

        wrapperView.addView(iv);
    }

    private ViewGroup setContentViewWithWrapper(int resContent) {
        ViewGroup decorView = (ViewGroup) this.getWindow().getDecorView();
        ViewGroup decorChild = (ViewGroup) decorView.getChildAt(0);
        // Removing decorChild, we'll add it back soon
        decorView.removeAllViews();

        LayoutInflater inflater;
        inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final ViewGroup wrapperView = (ViewGroup) inflater.inflate(R.layout.above_root_layout, null);

        // Now we are rebuilding the DecorView, but this time we
        // have our wrapper view to stand between the real content and the decor
        decorView.addView(wrapperView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        wrapperView.addView(decorChild, decorChild.getLayoutParams());

        LayoutInflater.from(this).inflate(resContent, (ViewGroup) (decorChild).getChildAt(decorChild.getChildCount() - 1), true);

        return wrapperView;
    }

    @Override
    protected void onResume() {
        super.onResume();
        decorateWindow();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate called " + savedInstanceState);
        super.onCreate(savedInstanceState);
        decorateWindow();
        setContentViewWithWrapper(R.layout.activity_card_table);
        decorateActionBar();

        Display display = this.getWindowManager().getDefaultDisplay();
        display.getSize(displaySize);

        configProvider = new ConfigProvider();
        configProvider.init(this);
        ds = new Dictionaries(this);
        ds.init();
        ds.updateSelectedDictionary(configProvider.getChoosenDictionaries(ds.getRootName(), ds.getDefaultFilename()));
        if (ds.getCurrentDictionary().isEmpty()) {
            List<String> adefault = Collections.singletonList(ds.getDefaultFilename());
            ds.updateSelectedDictionary(adefault);
            configProvider.setChoosenDictionaries(ds.getRootName(), adefault);
        }
        menuHelper = new MenuHelper(this, ds, configProvider, new Runnable() {
            @Override
            public void run() {
                menuHelper.hideActionBar();
                shuffleLevel();
            }
        });
        menuHelper.onCreate(savedInstanceState);
        wordListHelper = new WordListHelper(this);
        menuHelper.setVisibilityListener(new VisibilityListener() {
            @Override
            public void notifyVisibilityChanged(boolean isShown) {
                if (isShown) {
                    wordListHelper.hideWordList();
                }
            }
        });
        wordListHelper.setVisibilityListener(new VisibilityListener() {
            @Override
            public void notifyVisibilityChanged(boolean isShown) {
                if (isShown) {
                    menuHelper.hideActionBar();
                }
            }
        });
        wordListHelper.onCreate(savedInstanceState);
        levelDoneHelper = new LevelDoneStaticHelper(this);
        levelDoneHelper.onCreate(savedInstanceState);
        autoplayHelper = new AutoplayHelper(this);
        autoplayHelper.onCreate(savedInstanceState);
        MultipleOnTouchListener multipleOnTouchListener = new MultipleOnTouchListener();
        multipleOnTouchListener.addOnTouchListener(menuHelper.new ShowActionBarListener());
        multipleOnTouchListener.addOnTouchListener(wordListHelper.new ShowWordListListener());
        final ViewGroup gridview = (ViewGroup) findViewById(R.id.gridview1);
        gridview.setOnTouchListener(multipleOnTouchListener);
        gridview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "CardTableActivity onClick");
                Level l = level;
                if (l != null) {
                    l.cardTableClickListener.onClick(v);
                }
            }
        });
        menuHelper.hideActionBar();
        handler = new Handler();
        shuffleLevel();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menuHelper.onCreateOptionsMenu(menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        Log.d(TAG, "onSaveInstanceState");
        saveStateController.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState");
        saveStateController.onRestoreInstanceState(savedInstanceState);
    }

    public AbstractLevelAutoPlayer.Listener getAutoPlayerListener() {
        return autoplayHelper.autoPlayerListener;
    }

    public int getActionBarFullHeight() {
        return menuHelper.getActionBarFullHeight();
    }

    public void forceNextLevel(android.view.MenuItem mi) {
//        Toast.makeText(this, "forceNextLevel clc", Toast.LENGTH_SHORT).show();
        menuHelper.hideActionBar();
        shuffleLevel();
    }

    private void shuffleLevel() {
        if (level != null) {
            level.levelController.destroy(new Runnable() {
                @Override
                public void run() {
                    internalShuffleLevel();
                }
            });
        } else {
            internalShuffleLevel();
        }
    }

    private Level nextLevel() {
        Level l = DictionaryLevelBuilder.prepareLevelBuilder(this, ds.getCurrentDictionary(), configProvider.getSelectedLevelSize()).nextLevel();
        return l;
    }

    private void internalShuffleLevel() {
        hideLevelCompleteDialog();
        final ViewGroup alayout = (ViewGroup) findViewById(R.id.mainFrameLayout);
        final ViewGroup layout = (ViewGroup) alayout.findViewById(R.id.gridview1);
        layout.removeAllViews();

        boolean restored = true;
        level = saveStateController.nextLevel();
        if (level == null) {
            level = nextLevel();
            restored = false;
        }
        ViewResolver vr = new ViewResolver(level, layout);
        level.setViewResolver(vr);

        if (layout instanceof CardTableLayout) {
            ((CardTableLayout) layout).setXPcs(level.getPuzzleDimension().x);
            ((CardTableLayout) layout).setYPcs(level.getPuzzleDimension().y);
        }

        for (int i = 0; i < level.getSize(); i++) {
            View v = level.constructView(i);
            layout.addView(v);
        }
        layout.invalidate();
        wordListHelper.fill(level.getPairs());

        Runnable done = new Runnable() {
            @Override
            public void run() {
                level.skipViewClicks = false;
            }
        };

        if (restored) {
            done.run();
            return;
        }
        StartLevelAnimator startLevelAnimator = new StartLevelAnimator(this, level);
        startLevelAnimator.animateLevelPopulation(done);

        Log.d(TAG, "shuffleLevel finished");
    }

    public void optionDictionaryClicked(android.view.MenuItem mi) {
        menuHelper.optionDictionaryClicked(mi);
    }

    public void showLevelCompleteDialog() {
        levelDoneHelper.showLevelCompleteDialog(level.getLevelCompleteInfo());
    }

    public void hideLevelCompleteDialog() {
        levelDoneHelper.hideLevelCompleteDialog();
    }

    public void menuSwipeHintTopClicked(View v) {
        menuHelper.menuSwipeHintTopClicked(v);
    }

    public void menuSwipeHintRightClicked(View v) {
        wordListHelper.menuSwipeHintRightClicked(v);
    }

    public void menuAutoplayButtonClicked(View v) {
        menuAutoplayButtonClicked();
    }

    public void menuAutoplayButtonClicked() {
        wordListHelper.hideWordList();
        menuHelper.hideActionBar();
        autoplayHelper.menuAutoplayButtonClicked();

        if (autoplayHelper.isStarted()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    public void menuAutoplayButtonClicked(android.view.MenuItem mi) {
        menuAutoplayButtonClicked();
    }

    //    <!--todo debug purposes-->
    public void _dbgLeft(android.view.MenuItem mi) {
        Toast.makeText(this, "_dbgLeft clc", Toast.LENGTH_SHORT).show();
        level = DictionaryLevelBuilder.prepareLevelBuilder(this, ds.getCurrentDictionary(), configProvider.getSelectedLevelSize()).previousLevel();
        final GridView gridview = (GridView) findViewById(R.id.gridview1);
        gridview.invalidate();
    }

    //    <!--todo debug purposes-->
    public void _dbgShowComplete(android.view.MenuItem mi) {
        levelDoneHelper._dbgShowComplete(level.getLevelCompleteInfo());
    }

    public void showAboutBox(android.view.MenuItem mi) {
        AlertDialog alertDialog = MyOtherAlertDialog.create(this);
        alertDialog.show();
    }

    public static class MyOtherAlertDialog {
        public static AlertDialog create(Context context) {
            final TextView message = new TextView(context);
            final SpannableString s =
//                    new SpannableString(context.getText(R.string.dialog_message));
                    new SpannableString("MY MESSAGE HERE www.google.com    abcde  aabcde@qwerty.com 123456");
            Linkify.addLinks(s, Linkify.WEB_URLS | Linkify.EMAIL_ADDRESSES);
            message.setText(s);
            message.setMovementMethod(LinkMovementMethod.getInstance());

            return new AlertDialog.Builder(context)
//                    .setTitle(R.string.dialog_title)
                    .setTitle("About Deumemo")
                    .setCancelable(true)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("OK", null)
                    .setView(message)
                    .create();
        }
    }
}

