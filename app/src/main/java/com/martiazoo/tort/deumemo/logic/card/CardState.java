package com.martiazoo.tort.deumemo.logic.card;

public enum CardState {
    TOP,
    BACK,
    HIDDEN,
}
