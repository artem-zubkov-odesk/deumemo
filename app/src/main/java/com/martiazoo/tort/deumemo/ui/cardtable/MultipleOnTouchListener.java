package com.martiazoo.tort.deumemo.ui.cardtable;

import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MultipleOnTouchListener implements View.OnTouchListener {

    private List<View.OnTouchListener> listeners = new ArrayList<>();

    public void addOnTouchListener(View.OnTouchListener listener) {
        listeners.add(listener);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        for (View.OnTouchListener listener : listeners) {
            if (listener.onTouch(v, event)) {
                return true;
            }
        }
        return false;
    }
}
