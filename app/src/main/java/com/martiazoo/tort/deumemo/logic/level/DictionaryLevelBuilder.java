package com.martiazoo.tort.deumemo.logic.level;

import android.content.Context;

import com.martiazoo.tort.deumemo.logic.dictionary.Dictionary;
import com.martiazoo.tort.deumemo.logic.dictionary.DictionaryIterator;
import com.martiazoo.tort.deumemo.logic.scope.LevelSize;
import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;

import java.util.List;

public class DictionaryLevelBuilder extends AbstractLevelBuilder {
    DictionaryIterator i;
    LevelSize ls;
    Context context;

    protected DictionaryLevelBuilder(Dictionary d, LevelSize ls, Context context) {
        this.ls = ls;
        this.context = context;
        i = new DictionaryIterator.ShuffleLoop();
        i.setDictionary(d);
    }

    public Level nextLevel() {
        throwInvalid();
        Level l = new Level(context);

        List<PairType> pairs = i.getNext(ls.getDimension().x * ls.getDimension().y / 2);
        l.init(ls.getDimension(), pairs, null);
        return l;
    }
}
