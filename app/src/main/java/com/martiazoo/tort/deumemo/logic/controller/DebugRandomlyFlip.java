package com.martiazoo.tort.deumemo.logic.controller;

import android.content.Context;
import android.view.View;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.logic.card.CardState;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;

import java.util.List;

public class DebugRandomlyFlip extends LevelControllerAdapter {
    @Override
    public void initialize(Context ctx, Level l, List<GFElement> all, ViewResolver vr) {
        super.initialize(ctx, l, all, vr);
        for (GFElement gfElement : all) {
            gfElement.setCardState(CardState.TOP);
        }
    }

    @Override
    public void heartbeat() {
        super.heartbeat();
        int rand = (int) (Math.random() * all.size());
        GFElement e = all.get(rand);
        if (e.getCardState() == null) {
            e.setCardState(CardState.TOP);
            View view = vr.toView(e);
            view.invalidate();
            return;
        }
        if (e.getCardState() == CardState.TOP) {
            e.setCardState(CardState.BACK);
            View view = vr.toView(e);
            view.invalidate();
            return;
        }
        if (e.getCardState() == CardState.BACK) {
            e.setCardState(CardState.HIDDEN);
            View view = vr.toView(e);
            view.invalidate();
            return;
        }
        if (e.getCardState() == CardState.HIDDEN) {
            e.setCardState(CardState.TOP);
            View view = vr.toView(e);
            view.invalidate();
            return;
        }
    }
}
