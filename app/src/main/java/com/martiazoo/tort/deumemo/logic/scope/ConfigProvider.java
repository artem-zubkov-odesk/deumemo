package com.martiazoo.tort.deumemo.logic.scope;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.martiazoo.tort.deumemo.ui.card.TextSizeEntry;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;

public class ConfigProvider {
    private static final String TAG = "ConfigProvider";
    private static final String APPLICATION_VERSION_PREFIX = ".v0";
    private SharedPreferences preferences;

    public void init(Context context) {
        preferences = context.getSharedPreferences("com.martiazoo.tort.deumemo.logic.scope.ConfigProvider" + APPLICATION_VERSION_PREFIX, Context.MODE_PRIVATE);
    }

    public LevelSize getSelectedLevelSize() {
        LevelSize adefault = LevelSize.M;
        String name = preferences.getString("LevelSize", adefault.name());
        try {
            return LevelSize.valueOf(name);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            return adefault;
        }
    }

    public void setSelectedLevelSize(LevelSize ls) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("LevelSize", ls.name());
        editor.apply();
    }

    public List<String> getChoosenDictionaries(String rootName, String defaultValue) {
        Set<String> set = preferences.getStringSet("Dictionary_Filenames_" + rootName, Collections.<String>emptySet());
        if (CollectionUtils.isEmpty(set)) {
            return Collections.singletonList(defaultValue);
        }
        return new ArrayList<>(set);
    }

    public void setChoosenDictionaries(String rootName, List<String> list) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet("Dictionary_Filenames_" + rootName, new LinkedHashSet<>(list));
        editor.apply();
    }

    /**
     * @param cardW normalized (intent substracted)
     * @param cardH normalized (intent substracted)
     * @param map:  key: lines.length, characters in word
     *              <br> map - : value: finally text size
     */
    public String tpPropertyKey(int cardW, int cardH, Map<Pair<Integer, Integer>, Integer> map) {
        return String.format("%dw_%dh_");

    }

    public List<TextSizeEntry> getCardSizeTextDimensions() {
        Set<String> set = preferences.getStringSet("CardSizeTextDimensions", Collections.<String>emptySet());
        List<TextSizeEntry> result = new ArrayList<>();
        if (CollectionUtils.isEmpty(set)) {
            return result;
        }
        for (String s : set) {
            TextSizeEntry e = TextSizeEntry.fromStorageEntry(s);
            result.add(e);
        }
        return result;
    }

    public void setCardSizeTextDimensions(List<TextSizeEntry> list) {
        Set<String> set = new LinkedHashSet<>();
        for (TextSizeEntry textSizeEntry : list) {
            String s = textSizeEntry.toStorageEntry();
            set.add(s);
        }
        SharedPreferences.Editor editor = preferences.edit();
        editor.putStringSet("CardSizeTextDimensions", set);
        editor.apply();
    }
}
