package com.martiazoo.tort.deumemo.util.threading.forceable;

import android.os.Handler;
import android.util.Log;
import com.martiazoo.tort.deumemo.util.threading.NullableRunnable;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class ForceableRunnableSequence {
    private static final String TAG = "FRSequence";
    private List<ForceableRunnable> list = new CopyOnWriteArrayList<>();
    private final Handler handler;
    private AtomicBoolean force = new AtomicBoolean();
    private AtomicBoolean forceForever = new AtomicBoolean();

    public ForceableRunnableSequence(Handler handler) {
        this.handler = handler;
    }

    /**
     * @return does something was forced
     */
    public boolean force(Runnable... done) {
        Log.d(TAG, "force requested" + this);
        force.set(true);
        final boolean forced = !list.isEmpty();
        int counter = 10;
        while (!list.isEmpty() && counter != 0) {
            for (ForceableRunnable r : list) {
                Log.d(TAG, "forcing of: " + r);
                r.run();
            }
            counter--;
        }
        force.set(false);
        NullableRunnable.getFirst(done).run();
        return forced;
    }

    public boolean forceForever(Runnable... done) {
        forceForever.set(true);
        return force(done);
    }

    public void register(final Runnable r, long delayMs, String message) {
        if (force.get() || forceForever.get()) {
            Log.d(TAG, "warnRegisterDuringForce: " + message);
            delayMs = 0;
        }
        ForceableRunnable wrapper = new ForceableRunnable(message) {
            @Override
            public void perform() {
                list.remove(this);
                r.run();
            }
        };
//        force();
        Log.d(TAG, "registering runnable: " + wrapper);
        list.add(wrapper);
        Log.d(TAG, "registering runnable: list size " + list.size() + " " + list);
        handler.postDelayed(wrapper, delayMs);
    }
}
