package com.martiazoo.tort.deumemo.ui.wordlist;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.martiazoo.tort.deumemo.ui.cardtable.ActivitySwipeDetector;
import com.martiazoo.tort.deumemo.R;
import com.martiazoo.tort.deumemo.Utils;
import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;
import com.martiazoo.tort.deumemo.ui.cardtable.CardTableActivity;

import java.util.*;

import static com.martiazoo.tort.deumemo.Utils.removeSpaces;

public class WordListHelper {
    private static final String TAG = "WordListHelper";
    private CardTableActivity ctx;
    private ListView listView;
    private ListView listViewBackground;
    private ArrayAdapter<PairType> listAdapter;
    private Animation leftToRightAnimation;
    private Animation rightToLeftAnimation;
    private VisibilityListener visibilityListener = VisibilityListener.DUMMY;
    int maximumAWidth;
    int maximumBWidth;

    public WordListHelper(CardTableActivity parent) {
        this.ctx = parent;
    }

    public void onCreate(Bundle savedInstanceState) {
        listView = (ListView) ctx.findViewById(R.id.wordListView);
        listViewBackground = (ListView) ctx.findViewById(R.id.wordListViewBackground);
        listAdapter = new WordListArrayAdapter(ctx, R.layout.word_list_entry, new ArrayList<PairType>());
        // Set the ArrayAdapter as the ListView's adapter.
        listView.setAdapter(listAdapter);
        leftToRightAnimation = AnimationUtils.loadAnimation(ctx, R.anim.move_from_right_to_left);
        rightToLeftAnimation = AnimationUtils.loadAnimation(ctx, R.anim.move_from_left_to_rigth);
        listView.setOnTouchListener(new ShowWordListListener());
        updateMenuHintImageSize();
    }

    public void menuSwipeHintRightClicked(View v) {
        triggerWordList();
    }

    public void updateMenuHintImageSize() {
        final ImageView iv = (ImageView) ctx.findViewById(R.id.menuSwipeHintRight);
        Point size = topMenuHintButtonSize();
        ViewGroup.LayoutParams params = iv.getLayoutParams();
        params.width = size.x;
        params.height = size.y;
        iv.setLayoutParams(params);
    }

    private Point topMenuHintButtonSize() {
        Point size = new Point();
        size.x = (int) (Utils.getActionBarHeight(ctx) * 0.7d);

        Drawable drawable = ctx.getResources().getDrawable(R.drawable.menu_swipe_hint_right_img_v2);
        double factor = ((double) drawable.getMinimumHeight()) / drawable.getMinimumWidth();
        size.y = (int) (factor * size.x);
        return size;
    }

    public void setVisibilityListener(VisibilityListener visibilityListener) {
        this.visibilityListener = visibilityListener;
    }

    public boolean isShowing() {
        return listView.isShown();
//        return listView.getVisibility() == View.VISIBLE;
    }

    public void hideWordList() {
        if (!isShowing()) {
            return;
        }
        listView.startAnimation(rightToLeftAnimation);
        listViewBackground.startAnimation(rightToLeftAnimation);
        listView.setVisibility(View.INVISIBLE);
        listViewBackground.setVisibility(View.INVISIBLE);
        visibilityListener.notifyVisibilityChanged(false);
    }

    public void hideWordListInternal() {
        listView.setVisibility(View.INVISIBLE);
        listViewBackground.setVisibility(View.INVISIBLE);
    }

    public void triggerWordList() {
        if (isShowing()) {
            hideWordList();
        } else {
            showWordList();
        }
    }

    public void showWordList() {
        if (isShowing()) {
            return;
        }
        listView.setVisibility(View.VISIBLE);
        listViewBackground.setVisibility(View.VISIBLE);
        listView.startAnimation(leftToRightAnimation);
        listViewBackground.startAnimation(leftToRightAnimation);
        visibilityListener.notifyVisibilityChanged(true);
//        animation.setAnimationListener(this);
    }

    public void fill(List<PairType> pairs) {
        hideWordListInternal();
        listAdapter.clear();
        updateMaximumAWidth(pairs);
        updateMaximumBWidth(pairs);
        updateLayoutWidth(pairs);

        List<PairType> sorted = new ArrayList<>(pairs);
        /*todo add a-b and index order into dictionary metadata*/
        Collections.sort(sorted, new AlphabeticalPairComparator(0));
        Log.d(TAG, "fill elements: " + sorted.size());
        listAdapter.addAll(sorted);
    }

    @SuppressWarnings("unchecked")
    void updateMaximumAWidth(List<PairType> pairs) {
        List<PairType> sorted = new ArrayList<>(pairs);
        Collections.sort(sorted, PairALengthComparator.INSTANCE);
        View v = constructPairView(sorted.get(0), ctx, 0, 0);
        v.measure(ctx.displaySize.x, ctx.displaySize.y);
        EditText rightView = (EditText) v.findViewById(R.id.rowTextViewA);
        maximumAWidth = rightView.getMeasuredWidth();
        maximumAWidth += 20;
        maximumAWidth = Math.min(maximumAWidth, ctx.displaySize.x / 2 - 50);
        maximumAWidth = Math.max(maximumAWidth, 50);
    }

    @SuppressWarnings("unchecked")
    void updateMaximumBWidth(List<PairType> pairs) {
        List<PairType> sorted = new ArrayList<>(pairs);
        Collections.sort(sorted, PairBLengthComparator.INSTANCE);
        View v = constructPairView(sorted.get(0), ctx, 0, 0);
        v.measure(ctx.displaySize.x, ctx.displaySize.y);
        EditText rightView = (EditText) v.findViewById(R.id.rowTextViewB);
        maximumBWidth = rightView.getMeasuredWidth();
        maximumBWidth += 20;
        maximumBWidth = Math.min(maximumBWidth, ctx.displaySize.x / 2 - 50);
        maximumBWidth = Math.max(maximumBWidth, 50);
    }

    @SuppressWarnings("unchecked")
    private void updateLayoutWidth(List<PairType> pairs) {
        if (pairs == null || pairs.isEmpty()) {
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.width = 100;
            listView.setLayoutParams(params);

            params = listViewBackground.getLayoutParams();
            params.width = 100;
            listViewBackground.setLayoutParams(params);
            return;
        }
        List<PairType> sorted = new ArrayList<>(pairs);
        Collections.sort(sorted, PairLengthComparator.INSTANCE);
        /*biggest string is first to include in size calculation*/
        View v = constructPairView(sorted.get(0), ctx, maximumAWidth, maximumBWidth);
        v.measure(ctx.displaySize.x, ctx.displaySize.y);
        int transparentBorderWidth = ctx.displaySize.y / 20;
        int width = v.getMeasuredWidth() + transparentBorderWidth;

        width = Math.min(width, ctx.displaySize.x);
        ViewGroup.LayoutParams params;
        params = listView.getLayoutParams();
        params.width = width;
        listView.setLayoutParams(params);

        params = listViewBackground.getLayoutParams();
        params.width = width;
        listViewBackground.setLayoutParams(params);
    }

    private ViewGroup constructPairView(PairType pair, Context context, int maximumAWidth, int maximumBWidth) {
        ViewGroup vg;
        vg = new LinearLayout(context);
        String inflater = Context.LAYOUT_INFLATER_SERVICE;
        LayoutInflater li;
        li = (LayoutInflater) context.getSystemService(inflater);
        li.inflate(R.layout.word_list_entry, vg, true);

        EditText aView = (EditText) vg.findViewById(R.id.rowTextViewA);
        EditText bView = (EditText) vg.findViewById(R.id.rowTextViewB);

        aView.setText(removeSpaces(pair.getA()));
        bView.setText(removeSpaces(pair.getB()));
        if (maximumAWidth != 0) {
            aView.setWidth(maximumAWidth);
        }
        if (maximumBWidth != 0) {
            bView.setWidth(maximumBWidth);
        }
        return vg;
    }

    private void colorizeZebra(ViewGroup vg, int position) {
        EditText aView = (EditText) vg.findViewById(R.id.rowTextViewA);
        EditText bView = (EditText) vg.findViewById(R.id.rowTextViewB);
        if (position % 2 != 0) {
            aView.setTextColor(ContextCompat.getColor(ctx, R.color.word_list_text_color_a_zebra));
            aView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.word_list_bg_color_a_zebra));

            bView.setTextColor(ContextCompat.getColor(ctx, R.color.word_list_text_color_b_zebra));
            bView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.word_list_bg_color_b_zebra));
        } else {
            aView.setTextColor(ContextCompat.getColor(ctx, R.color.word_list_text_color_a));
            aView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.word_list_bg_color_a));

            bView.setTextColor(ContextCompat.getColor(ctx, R.color.word_list_text_color_b));
            bView.setBackgroundColor(ContextCompat.getColor(ctx, R.color.word_list_bg_color_b));
        }
    }

    private class WordListArrayAdapter extends ArrayAdapter<PairType> {
        public WordListArrayAdapter(Context context, int resource, List<PairType> objects) {
            super(context, resource, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PairType item = getItem(position);
            ViewGroup vg = WordListHelper.this.constructPairView(item, getContext(), maximumAWidth, maximumBWidth);
            colorizeZebra(vg, position);
            return vg;
        }
    }

    public class ShowWordListListener extends ActivitySwipeDetector {
        @Override
        public boolean onLeftToRightSwipe() {
            super.onLeftToRightSwipe();
            triggerWordList();
            return true;
        }

        @Override
        public boolean onRightToLeftSwipe() {
            super.onRightToLeftSwipe();
            triggerWordList();
            return true;
        }

        @Override
        public boolean onSimpleTap() {
            if (isShowing()) {
                super.onSimpleTap();
                hideWordList();
                return true;
            }
            return false;
        }
    }

    private static class PairLengthComparator implements Comparator<PairType> {
        private static final Comparator INSTANCE = new PairLengthComparator();

        @Override
        public int compare(PairType lhs, PairType rhs) {
            int lLength = 0;
            if (lhs != null) {
                lLength += lhs.getA() != null ? lhs.getA().length() : 0;
                lLength += lhs.getB() != null ? lhs.getB().length() : 0;
            }
            int rLength = 0;
            if (rhs != null) {
                rLength += rhs.getA() != null ? rhs.getA().length() : 0;
                rLength += rhs.getB() != null ? rhs.getB().length() : 0;
            }
            int result = lLength - rLength;
            return result * -1;
        }
    }

    private static class PairALengthComparator implements Comparator<PairType> {
        private static final Comparator INSTANCE = new PairALengthComparator();

        @Override
        public int compare(PairType lhs, PairType rhs) {
            int lLength = 0;
            if (lhs != null) {
                lLength += lhs.getA() != null ? lhs.getA().length() : 0;
//                lLength += lhs.getB() != null ? lhs.getB().length() : 0;
            }
            int rLength = 0;
            if (rhs != null) {
                rLength += rhs.getA() != null ? rhs.getA().length() : 0;
//                rLength += rhs.getB() != null ? rhs.getB().length() : 0;
            }
            int result = lLength - rLength;
            return result * -1;
        }
    }

    private static class PairBLengthComparator implements Comparator<PairType> {
        private static final Comparator INSTANCE = new PairBLengthComparator();

        @Override
        public int compare(PairType lhs, PairType rhs) {
            int lLength = 0;
            if (lhs != null) {
//                lLength += lhs.getA() != null ? lhs.getA().length() : 0;
                lLength += lhs.getB() != null ? lhs.getB().length() : 0;
            }
            int rLength = 0;
            if (rhs != null) {
//                rLength += rhs.getA() != null ? rhs.getA().length() : 0;
                rLength += rhs.getB() != null ? rhs.getB().length() : 0;
            }
            int result = lLength - rLength;
            return result * -1;
        }
    }

    private static class AlphabeticalPairComparator implements Comparator<PairType> {
        private final AlphabeticalStringComparator asc;

        public AlphabeticalPairComparator(int wordIndex) {
            asc = new AlphabeticalStringComparator(wordIndex);
        }

        @Override
        public int compare(PairType lhs, PairType rhs) {
            if (lhs == null || rhs == null) {
                throw new IllegalArgumentException();
            }
            return asc.compare(lhs.getB(), rhs.getB());
        }
    }

    private static class AlphabeticalStringComparator extends StringComparator {
        private final int wordIndex;

        public AlphabeticalStringComparator(int wordIndex) {
            this.wordIndex = wordIndex;
            if (wordIndex < 0) {
                throw new IllegalArgumentException();
            }
        }

        @Override
        public int compare(String lhs, String rhs) {
            if (lhs == null || rhs == null) {
                throw new IllegalArgumentException("Strings in dictionary cannot be null");
            }
            if (wordIndex == 0) {
                return super.compare(lhs, rhs);
            }
            String[] lefts = lhs.split(" ");
            String[] rights = lhs.split(" ");

            int i;
            i = Math.min(wordIndex, lefts.length - 1);
            lhs = lefts[i];
            i = Math.min(wordIndex, rights.length - 1);
            rhs = rights[i];

            return super.compare(lhs, rhs);
        }
    }

    private static class StringComparator implements Comparator<String> {
        @Override
        public int compare(String lhs, String rhs) {
            return String.CASE_INSENSITIVE_ORDER.compare(lhs, rhs);
        }
    }
}


