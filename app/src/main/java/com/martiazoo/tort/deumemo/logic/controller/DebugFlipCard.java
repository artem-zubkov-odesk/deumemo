package com.martiazoo.tort.deumemo.logic.controller;

import android.content.Context;
import android.util.Log;
import android.view.View;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.logic.card.CardState;
import com.martiazoo.tort.deumemo.ui.card.CardView;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class DebugFlipCard extends LevelControllerAdapter {
    private static final String TAG = "DebugFlipCard";
    final Map<GFElement, AtomicInteger> map = new HashMap<>();

    @Override
    public void initialize(Context ctx, Level l, List<GFElement> all, ViewResolver vr) {
        super.initialize(ctx, l, all, vr);
        for (GFElement e : all) {
            e.setCardState(CardState.TOP);
        }
        for (GFElement e : all) {
            map.put(e, new AtomicInteger());
        }
    }

    @Override
    public void elementClicked(GFElement e) {
        int state = map.get(e).getAndIncrement();
        state %= 4;
        Log.d(TAG, "state: " + state);
        if (state == 0) {
            /*TOP to BACK*/
            Log.d(TAG, "TOP to BACK");
            CardView v = vr.toView(e);
            TwoPassFlow.animateCardHide(v, pool);
            return;
        }
        if (state == 1) {
            /*BACK to HIDDEN*/
            Log.d(TAG, "BACK to HIDDEN");
            final CardView v = vr.toView(e);
            TwoPassFlow.animateCardDisappear(v, new Runnable() {
                @Override
                public void run() {
                    Log.d(TAG, "v is back to visible");
                    v.setVisibility(View.VISIBLE);
                    v.invalidate();
                }
            });

            return;
        }
        if (state == 2) {
            /*HIDDEN to BACK*/
            Log.d(TAG, "HIDDEN to BACK");
            CardView v = vr.toView(e);
            e.setCardState(CardState.BACK);
            LevelControllerAdapter.animateCardAppear(v, 0);
            return;
        }
        if (state == 3) {
            /*BACK to TOP*/
            Log.d(TAG, "BACK to TOP");
            CardView v = vr.toView(e);
            TwoPassFlow.animateCardOpened(v, pool);
            return;
        }
    }
}
