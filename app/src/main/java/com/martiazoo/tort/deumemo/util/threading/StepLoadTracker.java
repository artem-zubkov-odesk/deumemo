package com.martiazoo.tort.deumemo.util.threading;

import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class StepLoadTracker implements Runnable {
    //    protected static final Logger LOG = Logger.getLogger(StepLoadTracker.class);
    protected static final String TAG = "StepLoadTracker";
    private final long waitTimeoutMs;
    private final int steps;
    private final Runnable finishedCallback;
    private final AtomicBoolean isProcessed = new AtomicBoolean(false);
    private final AtomicInteger counter = new AtomicInteger();
    private final Handler t;

    public StepLoadTracker(int steps, @Nullable Runnable finishedCallback, long waitTimeoutMs, Handler t) {
//        super("StepLoadTracker from " + determineCodeCallPlace());
        this.waitTimeoutMs = waitTimeoutMs;
        this.steps = steps;
        this.finishedCallback = NullableRunnable.wrap(finishedCallback);
        this.t = t;
    }

    /**
     * without backup timer
     */
    public StepLoadTracker(int steps, @Nullable Runnable finishedCallback) {
        this(steps, finishedCallback, -1, null);
    }

    public final void waitLoading() {
        Log.d(TAG, "waiting for loading ... " + this);
        if (steps == 0) {
            internalProcess("continue thread, no steps for " + this);
            return;
        }
        if (waitTimeoutMs > 0) {
            t.postDelayed(new Runnable() {
                @Override
                public void run() {
                    internalProcess("Loading skipped by backup timer : " + this);
                }
            }, waitTimeoutMs);
        }
    }

    public final void stepLoaded() {
        int counterValue = counter.incrementAndGet();
        Log.d(TAG, String.format("Step loaded %d/%d : %s", counterValue, steps, this));
        if (counterValue >= steps) {
            internalProcess("Loading completed normally : " + this);
        }
    }

    @Override
    public void run() {
        stepLoaded();
    }

    private void internalProcess(String message) {
        if (isProcessed.compareAndSet(false, true)) {
            Log.d(TAG, message);
            finishedCallback.run();
        }
    }

    public int getSteps() {
        return steps;
    }

    public int getCounter() {
        return counter.get();
    }

    @Override
    public String toString() {
        return super.toString() + "{" +
                "steps=" + steps +
                ", waitTimeoutMs=" + waitTimeoutMs +
                ", counter=" + counter +
                '}';
    }

    private static String determineCodeCallPlace() {
//        if (!LOG.isDebugEnabled()) {
//            return "Available only on log4j level 'debug'";
//        }
//        String ccp = AuxiliaryUtils.determineCodeCallPlace(4);
//        if (ccp.contains("<init>")) {
//            ccp = AuxiliaryUtils.determineCodeCallPlace(5);
//        }
//        return ccp;
        return "N/A";
    }
}