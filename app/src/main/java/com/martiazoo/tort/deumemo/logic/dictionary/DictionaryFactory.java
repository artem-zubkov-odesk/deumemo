package com.martiazoo.tort.deumemo.logic.dictionary;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DictionaryFactory extends PairCollectionLoader {
    public DictionaryFactory(Context ctx, String subDirectory) {
        super(ctx, subDirectory);
    }

    Dictionary createDictionary(List<String> filenames) {
        List<PairCollection> list = new ArrayList<>();
        for (String s : filenames) {
            PairCollection pc = getPairCollection(s);
            list.add(pc);
        }
        list.removeAll(Collections.<PairCollection>singleton(null));
        Dictionary d = new Dictionary(list);
        return d;
    }
}
