package com.martiazoo.tort.deumemo.ui.card;

public class TextSizeEntry {
    public final MapKey mapKey = new MapKey();
    int cardW;
    int cardH;
    int lines;
    int characters;
    int textSize;

    public TextSizeEntry(int cardW, int cardH, int lines, int characters) {
        this.characters = characters;
        this.lines = lines;
        this.cardH = cardH;
        this.cardW = cardW;
    }

    @Override
    public String toString() {
        return "TextSizeEntry " + toStorageEntry();
    }

    public String toStorageEntry() {
        return "w" + cardW + "_" +
                "h" + cardH + "_" +
                "l" + lines + "_" +
                "c" + characters +
                "=" + textSize;
    }

    static public TextSizeEntry fromStorageEntry(String line) {
        String s;
        s = line.substring(line.indexOf("w") + 1);
        s = s.substring(0, s.indexOf("_h"));
        int cardW = Integer.valueOf(s);

        s = line.substring(line.indexOf("h") + 1);
        s = s.substring(0, s.indexOf("_l"));
        int cardH = Integer.valueOf(s);

        s = line.substring(line.indexOf("l") + 1);
        s = s.substring(0, s.indexOf("_c"));
        int lines = Integer.valueOf(s);

        s = line.substring(line.indexOf("c") + 1);
        s = s.substring(0, s.indexOf("="));
        int characters = Integer.valueOf(s);

        s = line.substring(line.indexOf("=") + 1);
        int textSize = Integer.valueOf(s);
        TextSizeEntry e = new TextSizeEntry(cardW, cardH, lines, characters);
        e.textSize = textSize;
        return e;
    }


    class MapKey {
        TextSizeEntry getParent() {
            return TextSizeEntry.this;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof MapKey)) return false;

            MapKey that = (MapKey) o;

            if (cardW != that.getParent().cardW) return false;
            if (cardH != that.getParent().cardH) return false;
            if (lines != that.getParent().lines) return false;
            return characters == that.getParent().characters;
        }

        @Override
        public int hashCode() {
            int result = cardW;
            result = 31 * result + cardH;
            result = 31 * result + lines;
            result = 31 * result + characters;
            return result;
        }

        @Override
        public String toString() {
            return "MapKey{" +
                    "textSizeEntry=" + getParent() +
                    '}';
        }
    }
}