package com.martiazoo.tort.deumemo.util;

import com.google.common.base.Function;

public abstract class SingleCachingConverter<A, B> implements Function<A, B> {
    protected A a;
    protected B b;
    private boolean isCalled;

    @Override
    public B apply(A a) {
        /*attempting nulls and whatever for first time*/
        if (!isCalled) {
            isCalled = true;
            this.a = a;
            this.b = doApply(a);
            return b;
        }
        if (this.a == a) {
            return b;
        }
        if (this.a != null && this.a.equals(a)) {
            return b;
        }
        this.a = a;
        this.b = doApply(a);
        return b;
    }

    abstract public B doApply(A input);
}
