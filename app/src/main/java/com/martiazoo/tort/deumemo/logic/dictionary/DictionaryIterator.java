package com.martiazoo.tort.deumemo.logic.dictionary;

import com.martiazoo.tort.deumemo.logic.xml.generated.PairType;

import org.apache.commons.collections4.IteratorUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public interface DictionaryIterator {
    public Dictionary getDictionary();
    public void setDictionary(Dictionary d);

    public List<PairType> getNext(int count);

    static abstract class Abstract implements DictionaryIterator {
        Dictionary d;

        @Override
        public void setDictionary(Dictionary d) {
            this.d = d;
            if (d.isEmpty()) {
                throw new IllegalStateException("empty collection in dictionary");
            }
        }

        public Dictionary getDictionary() {
            return d;
        }
    }

    static class Same extends Abstract {
        @Override
        public List<PairType> getNext(int count) {
            if (count == 0) {
                throw new IllegalArgumentException("nonsence call");
            }
            List<PairType> result = new ArrayList<>(count);
            Iterator<PairType> i = d.iterator();
            while (true) {
                while (i.hasNext()) {
                    PairType pairType = i.next();
                    result.add(pairType);
                    if (result.size() == count) {
                        return result;
                    }
                }
                i = d.iterator();
                if (result.size() == 0) {
                    throw new RuntimeException("empty collection in dictionary");
                }
            }
        }
    }

    static class Loop extends Abstract {
        private Iterator<PairType> i;

        @Override
        public List<PairType> getNext(int count) {
            if (count == 0) {
                throw new IllegalArgumentException("nonsense call");
            }
            List<PairType> result = new ArrayList<>(count);
            if (i == null) {
                i = getDictionaryIterator();
            }
            while (true) {
                while (i.hasNext()) {
                    PairType pairType = i.next();
                    result.add(pairType);
                    if (result.size() == count) {
                        return result;
                    }
                }
                i = getDictionaryIterator();
            }
        }

        protected Iterator<PairType> getDictionaryIterator() {
            return d.iterator();
        }
    }

    static class ShuffleLoop extends Loop {
        private List<PairType> shuffled;

        @Override
        public void setDictionary(Dictionary d) {
            super.setDictionary(d);
            shuffled = IteratorUtils.toList(d.iterator());
            Collections.shuffle(shuffled);
        }

        protected Iterator<PairType> getDictionaryIterator() {
            return shuffled.iterator();
        }
    }
}
