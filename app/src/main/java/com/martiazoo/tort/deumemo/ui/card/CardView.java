package com.martiazoo.tort.deumemo.ui.card;

import android.content.Context;
import android.graphics.*;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import com.martiazoo.tort.deumemo.logic.card.CardState;
import com.martiazoo.tort.deumemo.logic.card.GFElement;

public class CardView extends View {
    private static final String TAG = "GFTextView";
    private static int COUNTER = 0;

    /*no locks required as UI tread invokes draw sequentially*/
    private static final Paint SELECTION_PAINT;

    static {
        SELECTION_PAINT = new Paint(Paint.ANTI_ALIAS_FLAG);
        SELECTION_PAINT.setColor(Color.BLACK);
        SELECTION_PAINT.setStrokeWidth(5);
        SELECTION_PAINT.setStyle(Paint.Style.STROKE);
    }

    private Context c;
    private GFElement el;
    private int position;

    private CardTextRenderer cardTextRenderer;
    private CardBitmapRenderer cardBitmapRenderer;
    private Bitmap cachedFrontBitmap;

    public CardView(Context context, CardTextRenderer cardTextRenderer, CardBitmapRenderer cardBitmapRenderer) {
        super(context);
        c = context;
        this.cardTextRenderer = cardTextRenderer;
        this.cardBitmapRenderer = cardBitmapRenderer;
        Log.d(TAG, "GFTextView created: " + COUNTER++);
    }

    public CardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        c = context;
        Log.d(TAG, "GFTextView created: " + COUNTER++);
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public GFElement getElement() {
        return el;
    }

    public void setElement(GFElement el) {
        this.el = el;
    }

    public void init() {
        setPadding(5, 5, 5, 5);
    }

    @Override
    public void invalidate() {
        super.invalidate();
        /*this is to clean up after animation is done*/
        setBackgroundColor(android.graphics.Color.TRANSPARENT);
    }

    void drawBack(Canvas canvas) {
        cardBitmapRenderer.notifyHeight(getHeight());
        cardBitmapRenderer.notifyWidth(getWidth());
        cardBitmapRenderer.notifyTextPaint(cardTextRenderer.textPaint);
        cardBitmapRenderer.tryInvalidate();
        Bitmap b = cardBitmapRenderer.getBackBitmap();
        canvas.drawBitmap(b, new Rect(0, 0, b.getWidth(), b.getHeight()),
                new Rect(0, 0, canvas.getWidth(), canvas.getHeight()), cardTextRenderer.textPaint);
    }

    void drawFront(Canvas canvas) {
        if (cachedFrontBitmap == null) {
            Bitmap output_bitmap = Bitmap.createBitmap(getWidth(), getHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(output_bitmap);

            Bitmap b;
            cardBitmapRenderer.notifyWidth(getWidth());
            cardBitmapRenderer.notifyHeight(getHeight());
            cardBitmapRenderer.notifyTextPaint(cardTextRenderer.textPaint);
            cardBitmapRenderer.tryInvalidate();
            b = cardBitmapRenderer.getFrontBitmap();

            c.drawBitmap(b, new Rect(0, 0, b.getWidth(), b.getHeight()),
                    new Rect(0, 0, c.getWidth(), c.getHeight()), cardTextRenderer.textPaint);

            cardTextRenderer.notifyCardWidth(getWidth());
            cardTextRenderer.notifyCardHeight(getHeight());
            int minIndent = Math.min(cardBitmapRenderer.getXIntent(), cardBitmapRenderer.getYIntent());
            cardTextRenderer.notifyCardWidth(c.getWidth() - cardBitmapRenderer.getXIntent() - minIndent);
            cardTextRenderer.notifyCardHeight(c.getHeight() - cardBitmapRenderer.getYIntent() - minIndent);
            b = cardTextRenderer.getFrontBitmap(el);
            c.drawBitmap(b, (c.getWidth() - b.getWidth()) / 2, (c.getHeight() - b.getHeight()) / 2, cardTextRenderer.textPaint);

            cachedFrontBitmap = output_bitmap;
        }
        canvas.drawBitmap(cachedFrontBitmap, 0, 0, cardTextRenderer.textPaint);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (cachedFrontBitmap != null && !cachedFrontBitmap.isRecycled()) {
            cachedFrontBitmap.recycle();
            cachedFrontBitmap = null;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        if (getWidth() <= 0 || getHeight() <= 0) {/*device is locked*/
            return;
        }
        if (el.getCardState() == CardState.HIDDEN) {
            super.draw(canvas);
            return;
        }
        if (el.getCardState() == CardState.BACK) {
            drawBack(canvas);
            return;
        }
        drawFront(canvas);
    }
}
