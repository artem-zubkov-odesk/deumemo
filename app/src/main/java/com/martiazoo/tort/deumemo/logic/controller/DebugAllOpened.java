package com.martiazoo.tort.deumemo.logic.controller;

import android.content.Context;
import com.martiazoo.tort.deumemo.logic.card.GFElement;
import com.martiazoo.tort.deumemo.logic.level.Level;
import com.martiazoo.tort.deumemo.logic.card.CardState;
import com.martiazoo.tort.deumemo.ui.card.ViewResolver;

import java.util.List;

public class DebugAllOpened extends LevelControllerAdapter {
    @Override
    public void initialize(Context ctx, Level l, List<GFElement> all, ViewResolver vr) {
        super.initialize(ctx, l, all, vr);
        for (GFElement gfElement : all) {
            gfElement.setCardState(CardState.TOP);
        }
    }
}
