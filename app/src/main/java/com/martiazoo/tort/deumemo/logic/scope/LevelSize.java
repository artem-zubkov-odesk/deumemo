package com.martiazoo.tort.deumemo.logic.scope;

import android.graphics.Point;

public enum LevelSize {
    S(new Point(2, 3)),
    M(new Point(2, 4)),
    L(new Point(3, 4)),
    XL(new Point(4, 4)),;
//    XXL(new Point(4, 5)),;

    LevelSize(Point dimension) {
        this.dimension = dimension;
        int size = dimension.x * dimension.y;
        if (size % 2 != 0) {
            throw new IllegalArgumentException(String.format("odd number of elements %dx%d = %d"
                    , dimension.x, dimension.y, size));
        }
//        if (dimension.x < dimension.y) {
//            throw new IllegalArgumentException("Vertically-stretched cells are unsupported");
//        }
    }

    private final Point dimension;

    public Point getDimension() {
        return dimension;
    }
}