package com.martiazoo.tort.deumemo.util.threading;

import android.os.Handler;
import android.util.Log;

import java.util.concurrent.CopyOnWriteArrayList;

public class Heartbeat {
    private static final String TAG = "Heartbeat";
    private static final Heartbeat heartbeat = new Heartbeat();

    static {
        heartbeat.start(new Handler());
    }

    private Multiplier multiplier = new Multiplier();
    private Handler handler;
    private Runnable r = new Runnable() {/*todo executor service here*/
        @Override
        public void run() {
            try {
                multiplier.heartbeat();
            } catch (Throwable e) {
                Log.e(TAG, e.getMessage(), e);
            }
            try {
                handler.postDelayed(this, 1000);
            } catch (Exception e) {
                Log.e(TAG, e.getMessage(), e);
            }
        }
    };

    public void register(Listener listener) {
        multiplier.list.add(listener);
    }

    public void unregister(Listener listener) {
        multiplier.list.remove(listener);
    }

    private void start(Handler handler) {
        this.handler = handler;
        handler.postDelayed(r, 0);
    }

    public static Heartbeat get() {
        return heartbeat;
    }

    public interface Listener {
        void heartbeat();
    }

    static class Multiplier implements Listener {
        final CopyOnWriteArrayList<Listener> list = new CopyOnWriteArrayList<>();

        @Override
        public void heartbeat() {
            for (Listener listener : list) {
                try {
                    listener.heartbeat();
                } catch (Throwable e) {
                    Log.e(TAG, e.getMessage(), e);
                }
            }
        }
    }
}
